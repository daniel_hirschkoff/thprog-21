(*** TP1 : Introduction à Coq : formules, entiers et listes ***)

(* TD Men : Étienne Miquey / Enguerrand Prebet 

Adresse email des TDmen : [Prenom].[Nom]@ens-lyon.fr

Pour avoir les raccourcis comme il faut :
- Lancer "coqide" dans un terminal
- Edit > Preferences > Shortcuts > Modifiers for Navigation Menu
- Changer vers <alt> (selon les versions:
  - Taper Alt+W dans le champ, ou,
  - Laisser le bouton <alt> seul enfonçé)
- Cliquer sur OK
- Quitter coqide
- Relancer coqide *)

(* Ci-dessous, du code Coq à lire et compléter. N'hésitez pas, si vous
jugez cela nécessaire, à commencer par rejouer les preuves Coq vues en
cours (le code est disponible depuis la page www du cours). N'hésitez pas également 
à demander de l'aide à votre chargé de TP. *)

(*** Logique Simple ***)

(* Les deux tactiques centrales de Coq sont :

  [intro]
    L'équivalent de "soit x" et de "supposons que..."  dans les maths
    informelles, [intro] introduit un nouvel objet (entité sur
    laquelle on raisonne, ou hypothèse) dans le contexte.
    On peut nommer l'objet que l'on introduit, avec [intro x] ou
    [intro H], par exemple.
    La variante [intros] fait [intro] autant que possible, en
    choisissant (plus ou moins arbitrairement) des noms pour les
    hypothèses ainsi créées.
    
  [apply H]
    Permet d'invoquer l'hypothèse nommée H. *)

Lemma trivial1 : forall P : Prop, P -> P.
intro P. intro H. apply H. Qed.

(* À vous de jouer maintenant !
Prouvez les lemmes suivants en utilisant uniquement intro et apply*)

Lemma trivial2 : forall P Q : Prop, (P -> Q) -> P -> Q.
(* # *) intro P. intro Q. intro H1. intro H2. apply H1. apply H2. (* # *)
 Qed.


(* [intros H1 H2 H3] est un raccourci pour [intro H1. intro H2. intro H3.]. *)
(*    Vous pouvez maintenant l'utiliser. *)

Lemma trivial3 : forall P Q R : Prop,
  (P -> Q -> R) -> (P -> Q) -> P -> R.
  (* # *) 
  intros P Q R H1 H2 H3. 
  apply H1. apply H3.
  apply H2. apply H3.
  (* #*)
Qed.


(* Une dernière chose à propose de apply. Observez la preuve suivante, 
que vous pourriez avoir écrite sans difficulté. *)

Lemma trivial4: forall P, (forall x, P x) -> P 2.
  intros P H.
  apply H.
Qed.


(* À la deuxième ligne, on a appliqué l'hypothèse (H: forall x, P x) 
pour en déduire (P 2), qui ne coïncide pourtant pas avec l'hypothèse. 
En réalité, ce que Coq a fait pour vous, c'est qu'il a deviné que vous
vouliez appliquez l'hypothèse H dans le cas particulier où x vaut 2. 
C'est à dire: *)

Lemma trivial4': forall P, (forall x, P x) -> P 2.
  intros P H.
  Check (H 2).
  apply (H 2).
Qed.


(* Autrement dit, on peut (et même parfois on doit) préciser les arguments
 d'une hypothèse quand on l'applique.
Après avoir médité sur cette idée, prouver le lemme suivant: *)
Lemma apply1: forall (P : nat -> Prop), (forall x y, P x -> P y) -> P 2 -> P 3. 
  (* # *)
  intros P H H2.
  apply (H 2).
  apply H2.
  (* # *)
Qed.



(* Pour chaque connecteur logique, Coq fournit une tactique pour l'utiliser
quand il est en hypothèse et une tactique pour le "construire" quand
il est en but. Voici ce qui a été vu en cours pour la conjonction et
l'existentielle :

    connecteur    | pour utiliser |  pour construire
  ----------------|---------------|---------------------
      P /\ Q      |  destruct H.  |  split.
  exists x:nat, P |  destruct H.  |  exists 17.
*)

(* La disjonction est notée \/ en Coq.
   Pour prouver un but de la forme A \/ B, on dispose des deux tactiques "left." et "right.".
   Devinez ce qui se passe lorsque l'on a une hypothèse de la forme "H : A\/B"
   dans le but courant, et que l'on fait [destruct H].
   Vérifiez que vous avez deviné juste en prouvant les lemmes suivants. *)

Lemma conj1: forall P Q : Prop, P /\ Q -> P.
(* # *) intros P Q H. destruct H. apply H. (* # *) Qed.

Lemma conj2: forall P Q : Prop, P -> Q -> P /\ Q.
(* # *) intros P Q H1 H2. split. apply H1. apply H2. (* # *) Qed.

Lemma or1 : forall P Q : Prop, P -> P \/ Q.
  (* # *) intros P Q H. left. apply H. (* # *) Qed.

Lemma or2 : forall P Q R : Prop,
  P \/ Q -> (P -> R) -> (Q -> R) -> R.
(* # *) intros P Q R H1 H2 H3. destruct H1. apply H2. apply H. apply H3. apply H. (* # *) Qed.

(* Prouvez maintenant le lemme suivant.
   Vous aurez besoin pour cela des tactiques suivantes pour travailler sur les expressions entières : 

    [reflexivity] 
      Permet de prouver un but de la forme "a = a". Cette tactique peut aussi s'utiliser pour 
      d'autres relations reflexive.

    [simpl]
      Permet de simplifier une expression dans le but. On verra plus formellement plus tard 
      comment ça marche, mais pour l'instant vous pouvez voir ça comme une tactique permettant 
      de réécrire par exemple "2*2" en "4".  
*)
Lemma echauffement_or : forall x, (x=2 \/ x=4) -> exists y, x=2*y /\ (y=1 \/ y=2).
(* # *)
intros x H. destruct H.
  exists 1. split.
    simpl. apply H.
    left. reflexivity.
  exists 2. split.
    simpl. apply H.
    right. reflexivity.
(* # *)
Qed.

(* Retour sur la curryfication *)
(* Devinez l'énoncé du lemme suivant, et prouvez-le *)
Lemma curry3 : forall A B C D, ((A/\B/\C) -> D) -> (* # *) A -> B -> C -> D(* # *). 
(* # *) intros A B C D H1 H2 H3 H4. apply H1. split. apply H2. split. apply H3. apply H4. (* # *)Qed. 


(*** Arithmétique Simple sur les Entiers ***)

(* Inspirez-vous des définitions de plusnat et pred vues en cours 
pour définir la soustraction sur les naturels, qui satisfera la propriété n-(n+k)=0 *)

(* # *)
(** Correction : On considèrera plusieurs définitions alternative de minus que vous avez pu faire, 
selon si on match d'abord sur n ou sur m, ou sur les deux.  **)
(* # *)




Fixpoint minus (n m:nat) : nat :=(* # *)
  match n,m with
  | _ , 0  => n
  | 0 , S k  => 0
  | S i, S k => minus  i k
  end(* # *).

(* Prouvez le lemme suivant. Vous aurez besoin de la tactique suivante:
    [case n]
    Permet de séparer une preuve sur n en deux : d'abord le cas "n = 0", puis le cas "n = S n0". *)

Lemma minus_zero : forall n, minus n 0 = n.
(* # *) intro n. case n. simpl. reflexivity. intro m. simpl. reflexivity. (* # *) Qed.

(* Vous aurez besoin de la tactique suivante pour le prochain lemme : 
    
    [induction n]
    Permet de faire une preuve par récurrence sur l'entier n (ou une induction sur n). 
    Il vous faudra alors, comme en maths, prouver le cas "n=0" puis le cas "S n" en supposant la propriété vraie
pour n *)

(* Formellement, le principe d'induction est résumé par la formule suivante en Coq : *)
Check nat_ind.

(* Notez la différence avec la tactique [case] précédente. *)
Check nat_case.

(* Indice supplémentaire : Maintenant que vous avez prouvé le lemme "minus_zero", vous pouvez maintenant 
écrire [apply minus_zero] pour utiliser ce lemme. *)

Lemma plus_minus : forall n k, (minus (n + k) n) = k.
(* # *) 
intros n k. induction n. 
  simpl. apply minus_zero.
  simpl. apply IHn.
Qed.

(** Correction : Deuxième cas, pour les élèves ayant fait le pattern-matching dans l'autre sens.
On peut prendre exactement les mêmes preuves. **)

Fixpoint minus2 (n m:nat)  : nat :=
  match m with
  | 0 => n
  | S m' => match n with 
    | 0 => 0 
    | S n' => (minus2 n' m') 
    end 
  end.


Lemma minus2_zero : forall n, minus2 n 0 = n.
intros. case n. simpl. reflexivity. simpl. reflexivity. 
Qed. 

Lemma plus_minus2 : forall n k, (minus2 (n + k) n) = k.
intros n k. induction n. 
  simpl. apply minus2_zero.
  simpl. apply IHn.
Qed.

(** Correction : Et enfin, le double pattern matching (la syntaxe Coq oblige l'utilisation des virgules) *)

Fixpoint minus3 (n m:nat)  : nat :=
  match n,m with
  | 0,_ => 0
  | n,0 => n
  | S n',S m' => minus3 n' m'
end.

(** Correction : Encore une fois, on utilise exactement les mêmes preuves **)

Lemma minus3_zero : forall n, minus2 n 0 = n.
intros. case n. simpl. reflexivity. simpl. reflexivity. Qed. 

Lemma plus_minus3 : forall n k, (minus2 (n + k) n) = k.
intros n k. induction n. 
  simpl. apply minus2_zero.
  simpl. apply IHn.
(* # *)
Qed.


(*** Ordres et relations  ***)

(** Pour vous montrer qu'avec Coq on peut aussi faire des mathématiques,
on va chercher ici à prouver quelques résultats très simples sur les ordres. 
Pour commencer, on se donne un objet X fixé (la carrière de l'ensemble ordonné
que l'on va considérer, et l'on définit ce qu'est une relation sur X, à savoir
une proposition portant sur deux éléments de X. *)

Parameter X : Set.
Definition relation:= X -> X -> Prop.

(** Obversez qu'ici, les relations sont définies commes des fonctions.
On pourrait par exemple définir la relation d'ordre sur les entiers 
en utilisant le fait que n≤m ssi n-m = 0 : *)

Definition leq n m:=  n - m = 0.

(** On vérifie que leq a bien le bon type.*)
Check leq.

(** Alternativement, on peut utiliser (comme en OCaml) le mot-clé [fun] 
à travers la structure:

     Definition f:= fun x y z => ...

pour définir des fonctions. La définition précédente est ainsi équivalente à:
*)

Definition leq':=fun n m => n-m = 0.


(** Comme vous le savez sûrement déjà, un ordre est une relation réflexive,
 anti-symétrique et transitive. Définissez ici ces différentes notions. *)

Definition reflexive (R:relation):= (* ## *) forall x, R x x (* ## *).
Definition antisymmetric (R:relation):= (* ## *) forall x y, R x y -> R y x -> x = y (* ## *).
Definition transitive (R:relation):=(* # *) forall x y z, R x y -> R y z -> R x z(* # *).

Definition order (R:relation):= reflexive R /\ antisymmetric R /\ transitive R.


(** Dans la suite, si vous avez besoin de déplier une définition, vous
pouvez utiliser la tactique:
    [unfold f]
qui permet de remplacer dans le but le nom "f" définit plus haut par son 
expression. De même, vous pouvez déplier une opération dans l'une des 
hypothèses du contexte en utilisant:
    [unfold f in H]
*)


(** Pour commencer, on va montrer que l'ordre inversé (i.e. la relation 
"retournée") définit lui-même un ordre. 
Définissez ici ce qu'est le retournée d'une relation. *)

Definition reverse (R:relation):=(* # *)fun x y => R y x (* # *).
Check reverse.

(** On peut maintenant montrer que si une relation est 
(réflexive|anti-symétrique|transitive) sa relation inverse l'est aussi. *)

Lemma rev_refl (R:relation): reflexive R -> reflexive (reverse R).
  (* # *)  intros H. apply H. (* # *)
Qed.

Lemma rev_antisym (R:relation): antisymmetric R -> antisymmetric (reverse R).
  (* # *)  intros H.
  unfold reverse. unfold antisymmetric. (* Notez qu'on pourrait se passer de ces commandes. *)
  intros x y H1 H2. apply H. apply H2. apply H1.
  (* # *)
Qed.


Lemma rev_trans (R:relation): transitive R -> transitive (reverse R).
  (* # *)
  intros H x y z H1 H2.
  apply (H z y x).
  apply H2. apply H1.
(* # *)  
Qed.


(** On peut maintenant facilement montrer qu'un ordre inversé est 
lui-même un ordre. *)

Lemma rev_order (R:relation):  order R -> order (reverse R).
  (* # *)  
  intro H. destruct H. destruct H0.
  split.
  apply rev_refl. apply H.
  split. apply rev_antisym. apply H0.
  apply rev_trans. apply H1.
  (* # *)  
Qed.





(*** Fonctions sur les listes ***)

(* Une "incantation" pour avoir accès à la bibliothèque Coq traitant des listes *)
Require Export List.

(* Coq connaît les listes polymorphes : les "listes de A", pour un type A quelconque
  (liste de naturels, de booléens, de listes de naturels, de fonctions de type nat -> nat, ..) *)
Print list.

(* Pour faciliter les choses, on va fixer ici un type de listes *)
Parameter A : Set.

(* Ainsi, quand vous déclarez un type de listes, utilisez "list A" *)

(* Définissez une fonction "rev" qui renvoie le retournement de son argument, 
en utilisant la fonction "++" qui permet de concatener deux listes, 
puis prouvez que c'est une involution, en s'aidant de ces lemmes intermédiaires. *)

Fixpoint rev l : list A := (* # *)match l with 
  | nil => nil
  | a::r => (rev r) ++ (a::nil)
                           end.
Check rev.
(* # *)
(* Encore une fois, on peut utiliser le principe d'induction sur les listes [induction l] *)
Check list_ind.

(* On vous donne également quelques lemmes intérmédiaires en Coq *)
Check app_nil_end.
Check app_assoc.

(* Prouvez maintenant les lemmes suivants. Vous aurez besoin encore d'une nouvelle tactique :
    [rewrite H] 
    Si H est une hypothèse de la forme "a = b", [rewrite H] remplace dans le but toutes les 
    occurences de "a" par "b". Inversement, nous avons aussi:
    [rewrite <- H]
    Si H est une hypothèse de la forme "a = b", [rewrite H] remplace dans le but toutes les 
    occurences de "b" par "a". *)

Lemma rev_concat : forall xs ys, rev (xs ++ ys) = rev ys ++ rev xs.
(* # *)
intros. induction xs.
  simpl. apply app_nil_end.
  simpl. rewrite IHxs. rewrite <- app_assoc. reflexivity. 
(* # *)
Qed.

Lemma rev_singleton : forall xs x, rev (xs ++ (x :: nil)) = x :: rev xs.
(* # *)
intros. induction xs. 
  simpl. reflexivity.
  simpl. rewrite IHxs. simpl. reflexivity. 
(* # *)  
Qed.

Lemma rev_involution : forall l, rev (rev l) = l.
(* # *)
intros. induction l.
  simpl. reflexivity.
  simpl. rewrite rev_singleton. rewrite IHl. reflexivity.
(* # *)
Qed.

(* Ce n'est pas très économique de retourner des listes en appelant à
chaque fois "++". Trouver une solution plus rapide puis prouver
l'équivalence des deux. *)

(* # *)
(** Correction on utilise une fonction auxiliaire **)

Fixpoint revoptaux (l : list A) n := match l with 
  |nil => n
  |a::r => revoptaux r (a::n)
end.
Definition revopt l := revoptaux l nil.
(* # *)


(* N'hésitez pas à prouver des lemmes intérmédiaires, surtout si vous utilisez des 
fonctions auxiliaires... *)

(* # *)
(** Correction : Nous prouvons d'abord que la fonction auxiliaire fait bien ce pour quoi elle
est définie **)

Lemma revoptaux_correct : forall l l', revoptaux l l' = rev l ++ l'.
intro. induction l. (* Attention, ici, introduire l' puis faire l'induction ne fonctionne pas *) 
  simpl. reflexivity.
  simpl. intro. (* En effet, ici on applique IHl sur (a::l') et non pas l'*)
 rewrite IHl. rewrite <- app_assoc. simpl. reflexivity. Qed.

(* La même preuve en introduisant l' : *)

Lemma revoptaux_correct_fail : forall l l', revoptaux l l' = rev l ++ l'.
intros l l'. induction l.
  simpl. reflexivity.
  simpl. (* Et l'hypothèse d'induction n'est pas appliquable... *)
Abort.
(* # *)

Lemma revopt_correct : forall l, revopt l = rev l.
(* # *)
intros. unfold revopt. induction l. 
  simpl. reflexivity.
  simpl. rewrite revoptaux_correct. reflexivity.
(* # *)
Qed.


(* Voyez avec votre chargé de TP si vous avez programmé la version "optimisée" de rev. 
dans le cas contraire, introduisez rev' et montrez que les deux fonctions coïncident *)


(* # *)
(** Correction : La fin du TP était plus difficile. Il n'est pas important de regarder la correction, mais 
si jamais ça vous intéresse, ou si vous aviez essayé mais pas réussi, la voici. **)
(* # *)

(* Utiliser List.fold_left pour donner une définition alternative de la fonction qui 
  calcule la longueur d'une liste *)
Check List.fold_left.

Definition fold_length (l : list A) : nat :=
(* # *)  fold_left (fun n a => S n) l 0(* # *).

(* ## *)
(* On prouve alors que cette définition correspond bien à la taille.
  On aura besoin de ces lemmes intermédiaires sur la fonction + *)

Lemma plus_zero : forall n, n = n + 0.
  induction n.
  simpl. reflexivity. 
  simpl. rewrite <- IHn. reflexivity.
Qed.

Lemma plus_S : forall n m, n + S m = S (n + m).
  intro n. induction n.
  simpl. reflexivity.
  intro m. simpl. rewrite IHn. reflexivity.
Qed.

(* Ensuite on prouve un lemme intermédiaire pour traiter le troisième argument de fold_left *)
Lemma fold_aux : forall (l : list A) m, fold_left (fun n a => S n) l m = m + fold_left (fun n a => S n) l 0.
intro l. induction l.
  intros. simpl. apply plus_zero.
  intros. simpl. rewrite IHl. rewrite (IHl 1) (* Ici, on force Coq à utiliser IHl avec l'entier 1 *) . simpl.
    rewrite plus_S. reflexivity.
Qed.

Lemma fold_length_correct : forall l, length l = fold_length l.
intro l. unfold fold_length. induction l.
simpl. reflexivity.
simpl. rewrite IHl. rewrite (fold_aux l 1). simpl. reflexivity.
Qed.
(* ## *)

(* Définissez une fonction perms : nat -> list (list nat) telle que perms k 
  renvoie la liste de toutes les permutations de la liste [0;..;k] 
  Prouvez ensuite des tas de propriétés intéressantes de perms.
  Bonne Chance ! *)

(* ##### *)  
(** Ici, nous avons choisi plusieurs fonctions intermédiaires **)

(** Prends une liste de liste ll, et rajoute un a dans toutes les sous-listes de ll **)
Fixpoint cons2 (a : nat) (ll : list (list nat)) : list (list nat) := match ll with 
  |nil => nil
  |l::rr => (a::l)::(cons2 a rr)
end. 

(** Rajoute à la liste l l'entier n à toutes les positions possibles **)
Fixpoint insert (n : nat) (l : list nat) : list (list nat) := match l with 
  |nil => (n::nil)::nil
  |a::r => (n::(a::r))::(cons2 a (insert n r))
end.

(** Insère n dans toutes les sous-listes de ll, à toutes les positions possibles **)
Fixpoint insert2 (n : nat) (ll : list (list nat)) : list (list nat) := match ll with 
  |nil => nil
  |l::rr => (insert n l) ++ (insert2 n rr)
end.

Fixpoint perms k := match k with 
  | 0 => (0::nil)::nil
  | S k' => insert2 (S k') (perms k')
end.

(** Nous prouvons, par exemple, que dans perms k, toutes les sous-listes 
ont une taille k+1 **)

(** On définit d'abord ce prédicat : 
  all_size ll k indique que toutes les sous-listes de ll ont une taille k **)
Fixpoint all_size (ll : list (list nat)) k := match ll with 
  |nil => True 
  |a::rr => (length(a) = k) /\ (all_size rr k)
end.

(** Correction : On déroule maintenant les lemmes sur all_size sur toutes les fonctions intérmédiaires **)
Lemma Size_of_cons2 : forall a ll k, (all_size ll k) -> (all_size (cons2 a ll) (S k)).
intros. induction ll.
  simpl. trivial.
  simpl. split. 
    simpl in H. destruct H. rewrite H. reflexivity.
    destruct H. apply IHll. apply H0.
Qed.

Lemma Size_of_insert : forall n l, all_size (insert n l) (S (length l)).
intros. induction l. 
  simpl. split. reflexivity. trivial.
  simpl. split. reflexivity. apply Size_of_cons2. apply IHl. Qed.

Lemma all_size_app : forall l l' k, (all_size l k) -> (all_size l' k) -> (all_size (l ++l') k).
intro l. induction l.
  intros. simpl. apply H0.
  intros. simpl. split. 
    destruct H. apply H.
    apply IHl. destruct H. apply H1. apply H0.
Qed.


Lemma Size_of_insert2 : forall n ll k, (all_size ll k) -> (all_size (insert2 n ll) (S k)).
intros. induction ll.
  simpl. trivial.
  simpl. apply all_size_app. destruct H. rewrite <- H. apply Size_of_insert.
  apply IHll. destruct H. apply H0. Qed.

(** On peut enfin prouver le lemme final **)
Lemma Size_of_perms : forall k, all_size (perms k) (S k).
intros. induction k.
  simpl. split. reflexivity. trivial.
  simpl. apply Size_of_insert2. apply IHk. 
Qed.

(** Maintenant, on va montrer que l'ensemble des permutation est de taille factorielle **)

(** On travaille d'abord sur la taille des sous-listes **)
Lemma size_cons2 : forall n l, length(cons2 n l) = length l.
intros. induction l.
  simpl. reflexivity.
  simpl. rewrite IHl. reflexivity. 
Qed.

Lemma size_insert : forall n l, length(insert n l) = S (length l) .
intros. induction l.
  simpl. reflexivity.
  simpl. rewrite size_cons2. rewrite IHl. reflexivity. Qed.

Lemma length_app : forall A (l : list A) l', length (l ++ l') = length l + length(l').
intros A l. induction l.
  intros. simpl. reflexivity.
  intros. simpl. rewrite IHl. reflexivity.
Qed.

(** all_size nous sert ici à trouver la taille du résultat de insert2 **)
Lemma size_insert2 : forall n ll k, (all_size ll k) -> length(insert2 n ll) = (length(ll)) * S k.
intros. induction ll.
  simpl. reflexivity.
  simpl. rewrite length_app. rewrite size_insert. rewrite IHll. destruct H. rewrite H. simpl. reflexivity.
  destruct H. apply H0.
Qed.

Fixpoint fact n := match n with 
  | 0 => 1
  | S k => (fact k) * n
end.

(** Et enfin le lemme final **)
Lemma size_perm : forall k, length (perms k) = fact (S k).
intro. induction k.
  simpl. reflexivity.
  simpl. rewrite (size_insert2 (S k) (perms k) (S k)).
  rewrite IHk. simpl. reflexivity.
  apply Size_of_perms.
Qed.
  
(* ##### *)  
