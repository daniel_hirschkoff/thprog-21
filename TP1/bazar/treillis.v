
(**  * Ordres et relations  *)


(** Pour vous montrer qu'avec Coq on peut aussi faire des mathématiques,
on va chercher ici à prouver quelques résultats très simples sur les ordres. 
Pour commencer, on se donne un objet X fixé (la carrière de l'ensemble ordonné
que l'on va considérer, et l'on définit ce qu'est une relation sur X, à savoir
une proposition portant sur deux éléments de X. *)


Axiom (X:Type).
Definition relation:= X -> X -> Prop.


(** Comme vous le savez sûrement déjà, un ordre est une relation réflexive,
 anti-symmétrique et transitive. Définissez ici ces différentes notions. *)

Definition reflexive (R:relation):= forall x, R x x.
Definition antisymmetric (R:relation):= forall x y, R x y -> R y x -> x = y.
Definition transitive (R:relation):=forall x y z, R x y -> R y z -> R x z.

Definition order (R:relation):= reflexive R /\ antisymmetric R /\ transitive R.


(** Le retourné d'une relation*)

Definition reverse (R:relation):relation:= fun x y => R y x.


(** Le retournée d'une relation (refl|antisym|trans) l'est aussi *)
Axiom (Ord:relation).
Infix "≤" := Ord (at level 70).
Infix "≥":=(reverse Ord) (at level 70).

Lemma rev_refl: reflexive Ord -> reflexive (reverse Ord).
  intros H x.
  apply H.
Qed.
  
Lemma rev_antisym: antisymmetric Ord -> antisymmetric (reverse Ord).
  intros H x y H1 H2.
  apply H. apply H2. apply H1.
Qed.

Lemma rev_trans: transitive Ord -> transitive (reverse Ord).
  intros H x y z H1 H2.
  apply (H z y x H2 H1). 
Qed.


(** Le retourné d'un ordre est un ordre. *)

Lemma rev_order:  order Ord -> order (reverse Ord).
  intro H.
  destruct H as (refl,(antisym,trans)).
  repeat split.
  - apply (rev_refl refl).
  - apply (rev_antisym antisym).
  - apply (rev_trans trans).
Qed.
    





(** * Treillis *)


(** On se donne désormais un ordre fixé.*)

Axiom Ord_refl: reflexive Ord.
Axiom Ord_antisym: antisymmetric Ord.
Axiom Ord_trans: transitive Ord.


(** En mathématiques, un treillis est un ensemble ordonné dans lequel 
tout couple d'élements admet une borne supérieure et une borne inférieure. 
On va ici s'intéresser à une définition alternatives basée sur l'existence de
de deux lois internes, le "meet" ∧ et le "join" ∨, vérifiant certaines 
propriétés algébriques et relativement à l'ordre. *)

(** Un loi interne (binaire) est simplement défini comme une opération 
prenant deux éléments dans X pour en construire un troisième. *)

Definition internal:= X -> X -> X.

(** Définir ici les propriétés suivantes des lois internes : commutativité, 
associativité, idempotence, absorption (d'une loi par rapport à une autre). *)

Definition commutative (l:internal):= forall a b, l a b = l b a.
Definition associative (l:internal):= forall a b c, l (l a b) c = l a (l b c).
Definition idempotent  (l:internal):= forall a, l a a = a .
Definition absorptive  (l1 l2:internal):= forall a b, l1 a (l2 a b) = a.

(** On suppose désormais données deux lois internes, le meet et le join,
vérifiant ces propriétés. *)

Axiom meet:internal.
Axiom join:internal.  
Infix "∧":=meet (at level 1).
Infix "∨":=join (at level 1).

Axiom meet_commutative: commutative meet.
Axiom meet_associative: associative meet.
Axiom meet_absorptive : absorptive meet join.
Axiom meet_idempotent : idempotent meet.

Axiom join_commutative: commutative join.
Axiom join_associative: associative join.
Axiom join_absorptive : absorptive join meet.
Axiom join_idempotent : idempotent join.


(** De plus, on suppose que le "meet" est cohérent en tant que borne supérieure
 vis-à-vis de l'ordre, c'est-à-dire que a est plus petit que b ssi a est aussi 
le meet de a et b. *)

Axiom consistent :forall a b, a ≤ b <-> a = a ∧ b.


(** Cette définition est en fait équivalent à la notion duale pour le join. 
Pour montrer le prouver, on commence par prouver un premier lemme (attention, 
il s'agit ici de réfléchir comme vous le feriez dans un cours d'algèbre, commencer
par raisonner avec un papier et un crayon !). *)

Lemma meet_join:
    forall (a b:X), a = a ∧ b <-> b = a ∨ b.
  intros; split;intro H;rewrite H.
  - rewrite meet_commutative,join_commutative,join_absorptive. reflexivity.
  - rewrite meet_absorptive. reflexivity.
Qed.


Lemma join_consistent:
    forall a b, a ≤ b <-> b = a ∨ b.
Proof.
    intros.
    split; intro H.
    - apply meet_join. apply consistent. apply H.
    - apply consistent. apply meet_join. apply H.
Qed.



(** En particulier, ceci induit que si l'on considère l'ordre retourné, alors
il suffit d'inverser les meet et les joins pour obtenir une structure vérifiant
exactement les même axiomes. *)


Lemma rev_consistent:
  forall a b, a ≥ b <-> a = a ∨ b.
Proof.  
    intros.
    split;intros.
    - apply join_consistent in H.
    (* rewrite H at 1. *)
      rewrite H. rewrite join_commutative,join_associative,join_idempotent.
      reflexivity.
    - apply join_consistent.
      rewrite join_commutative.
      apply H.
Qed.


(** On peut maintenant vérifier que les axiomes donnés pour meet
définissent bien une borne supérieure. *)

Lemma meet_lb_l:
   forall a b,  a ∧ b ≤ a.
Proof.
  intros a b.
  apply consistent.
  rewrite meet_associative.
  rewrite (meet_commutative _ (b ∧ a)).
  rewrite meet_associative,meet_idempotent,meet_commutative.
  reflexivity.
Qed.


(** Évidemment, pour montrer la même chose à droite, on pourrait être
tenté de copier-coller la preuve précédente en adaptant ce qu'il faut.
Néanmoins, c'est profondément inélégant et vous ne feriez jamais cela
dans un cours de mathématiques. Essayez plutôt de le prouver en vous 
servant du résultat précédent. *)

Lemma meet_lb_r :
  forall a b : X,  a ∧ b ≤ b.
Proof.
  intros.
  rewrite meet_commutative.
  apply meet_lb_l.
Qed.



(** On peut finalement montrer le résultat attendu : 
a ∧ b est bien la plus grande borne inférieure de a et b. *)

Theorem meet_glb:
  forall a b : X, forall x, x ≤ a /\ x ≤ b <->  x ≤ a ∧ b.
Proof.
  intros;split;intros.
  - destruct H as (Ha,Hb).
    apply consistent.
    apply consistent in Ha.
    apply consistent in Hb.
    rewrite <- meet_associative.
    rewrite <- Ha.  exact Hb.
  - split.
    + apply (Ord_trans  _ (a ∧ b)). apply H.
      apply meet_lb_l.
    + apply (Ord_trans _ (a ∧ b));auto.
      apply meet_lb_r.
Qed.


(** On peut par ailleurs montrer que meet définit une opération
monotone respectivement à ses deux composantes. *)


Lemma meet_mon_l:
  forall a a' b, a ≤ a' -> a ∧ b ≤ a' ∧ b.
Proof.
  intros a a' b H.
  apply meet_glb. split.
  + apply (Ord_trans _ a _ (meet_lb_l a b) H). 
  + apply meet_lb_r.
Qed.

(** Là encore, fuyez la tentation du copié-collé, et servez-vous 
plutôt du lemme précédent. *)

Lemma meet_mon_r:
  forall a a' b, a≤a' -> b ∧ a ≤ b ∧ a'.
Proof.
  intros a a' b H. do 2 rewrite (meet_commutative b _). apply meet_mon_l. auto.
Qed.



(** On pourrait faire la même chose pour join et les bornes supérieures, 
mais ce sont exactement les mêmes preuves par dualité. Plus tard, vous serez 
capables d'automatiser cela pour ne pas avoir à refaire les preuves, mais pour 
le moment, il faudrait tout réécrire, ce que l'on a jusqu'ici soigneusement 
cherché à éviter, on ne va donc pas tout gâcher maintenant. *)
