(*** TP1 : Introduction à Coq : formules, entiers et listes ***)

(* TD Men : Étienne Miquey / Enguerrand Prebet 

Adresse email des TDmen : [Prenom].[Nom]@ens-lyon.fr

Pour avoir les raccourcis comme il faut :
- Lancer "coqide" dans un terminal
- Edit > Preferences > Shortcuts > Modifiers for Navigation Menu
- Changer vers <alt> (selon les versions:
  - Taper Alt+W dans le champ, ou,
  - Laisser le bouton <alt> seul enfonçé)
- Cliquer sur OK
- Quitter coqide
- Relancer coqide *)

(* Ci-dessous, du code Coq à lire et compléter. N'hésitez pas, si vous
jugez cela nécessaire, à commencer par rejouer les preuves Coq vues en
cours (le code est disponible depuis la page www du cours). N'hésitez pas également 
à demander de l'aide à votre chargé de TP. *)

(*** Logique Simple ***)

(* Les deux tactiques centrales de Coq sont :

  [intro]
    L'équivalent de "soit x" et de "supposons que..."  dans les maths
    informelles, [intro] introduit un nouvel objet (entité sur
    laquelle on raisonne, ou hypothèse) dans le contexte.
    On peut nommer l'objet que l'on introduit, avec [intro x] ou
    [intro H], par exemple.
    La variante [intros] fait [intro] autant que possible, en
    choisissant (plus ou moins arbitrairement) des noms pour les
    hypothèses ainsi créées.
    
  [apply H]
    Permet d'invoquer l'hypothèse nommée H. *)

Lemma trivial1 : forall P : Prop, P -> P.
intro P. intro H. apply H. Qed.

(* À vous de jouer maintenant !
Prouvez les lemmes suivants en utilisant uniquement intro et apply*)

Lemma trivial2 : forall P Q : Prop, (P -> Q) -> P -> Q.
Qed.


(* (*[intros H1 H2 H3] est un raccourci pour [intro H1. intro H2. intro H3.]. *)
(*    Vous pouvez maintenant l'utiliser.*) *)

Lemma trivial3 : forall P Q R : Prop,
  (P -> Q -> R) -> (P -> Q) -> P -> R.
Qed.


(* Une dernière chose à propose de apply. Observez la preuve suivante, 
que vous pourriez avoir écrite sans difficulté. *)

Lemma trivial4: forall P, (forall x, P x) -> P 2.
  intros P H.
  apply H.
Qed.


(* À la deuxième ligne, on a appliqué l'hypothèse (H: forall x, P x) 
pour en déduire (P 2), qui ne coïncide pourtant pas avec l'hypothèse. 
En réalité, ce que Coq a fait pour vous, c'est qu'il a deviné que vous
vouliez appliquez l'hypothèse H dans le cas particulier où x vaut 2. 
C'est à dire: *)

Lemma trivial4': forall P, (forall x, P x) -> P 2.
  intros P H.
  Check (H 2).
  apply (H 2).
Qed.


(* Autrement dit, on peut (et même parfois on doit) préciser les arguments
 d'une hypothèse quand on l'applique.
Après avoir médité sur cette idée, prouver le lemme suivant: *)

Lemma apply1: forall (P : nat -> Prop), (forall x y, P x -> P y) -> P 2 -> P 3.
Qed.




(* Pour chaque connecteur logique, Coq fournit une tactique pour l'utiliser
quand il est en hypothèse et une tactique pour le "construire" quand
il est en but. Voici ce qui a été vu en cours pour la conjonction et
l'existentielle :

    connecteur    | pour utiliser |  pour construire
  ----------------|---------------|---------------------
      P /\ Q      |  destruct H.  |  split.
  exists x:nat, P |  destruct H.  |  exists 17.
*)

(* La disjonction est notée \/ en Coq.
   Pour prouver un but de la forme A \/ B, on dispose des deux tactiques "left." et "right.".
   Devinez ce qui se passe lorsque l'on a une hypothèse de la forme "H : A\/B"
   dans le but courant, et que l'on fait [destruct H].
   Vérifiez que vous avez deviné juste en prouvant les lemmes suivants. *)

Lemma conj1: forall P Q : Prop, P /\ Q -> P.
Qed.

Lemma conj2: forall P Q : Prop, P -> Q -> P /\ Q.
Qed.

Lemma or1 : forall P Q : Prop, P -> P \/ Q.
Qed.

Lemma or2 : forall P Q R : Prop,
  P \/ Q -> (P -> R) -> (Q -> R) -> R.
Qed.

(* Prouvez maintenant le lemme suivant.
   Vous aurez besoin pour cela des tactiques suivantes pour travailler sur les expressions entières : 

    [reflexivity] 
      Permet de prouver un but de la forme "a = a". Cette tactique peut aussi s'utiliser pour 
      d'autres relations reflexive.

    [simpl]
      Permet de simplifier une expression dans le but. On verra plus formellement plus tard 
      comment ça marche, mais pour l'instant vous pouvez voir ça comme une tactique permettant 
      de réécrire par exemple "2*2" en "4".  
*)
Lemma echauffement_or : forall x, (x=2 \/ x=4) -> exists y, x=2*y /\ (y=1 \/ y=2).
Qed.

(* Retour sur la curryfication *)
(* Devinez l'énoncé du lemme suivant, et prouvez-le *)
Lemma curry3 : forall A B C D : Prop, ((A/\B/\C) -> D) -> (???).
Qed.

(*** Arithmétique Simple sur les Entiers ***)

(* Inspirez-vous des définitions de plusnat et pred vues en cours 
pour définir la soustraction sur les naturels, qui satisfera la propriété n-(n+k)=0 *)

Fixpoint minus (n m:nat) : nat := ...

(* Prouvez maintenant le lemme suivant. Vous aurez sûrement besoin de la tactique suivante:
    [case n]
    Permet de séparer une preuve sur n en deux : d'abord le cas "n = 0", puis le cas "n = S n0". *)

Lemma minus_zero : forall n, minus n 0 = n.
Qed.

(* Vous aurez besoin de la tactique suivante pour le prochain lemme : 
    
    [induction n]
    Permet de faire une preuve par récurrence sur l'entier n (ou une induction sur n). 
    Il vous faudra alors, comme en maths, prouver le cas "n=0" puis le cas "S n" en supposant la propriété vraie
pour n *)

(* Formellement, le principe d'induction est résumé par la formule suivante en Coq : *)
Check nat_ind.

(* Notez la différence avec la tactique [case] précédente. *)
Check nat_case.

(* Indice supplémentaire : Maintenant que vous avez prouvé le lemme "minus_zero", vous pouvez maintenant 
écrire [apply minus_zero] pour utiliser ce lemme. *)

Lemma plus_minus : forall n k, (minus (n + k) n) = k.
Qed.




(*** Ordres et relations  ***)


(** Pour vous montrer qu'avec Coq on peut aussi faire des mathématiques,
on va chercher ici à prouver quelques résultats très simples sur les ordres. 
Pour commencer, on se donne un objet X fixé (la carrière de l'ensemble ordonné
que l'on va considérer, et l'on définit ce qu'est une relation sur X, à savoir
une proposition portant sur deux éléments de X. *)


Parameter X : Set.
Definition relation:= X -> X -> Prop.

(** Obversez qu'ici, les relations sont définies commes des fonctions.
On pourrait par exemple définir la relation d'ordre sur les entiers 
en utilisant le fait que n≤m ssi n-m = 0 : *)

Definition leq n m:=  n - m = 0.

(** On vérifie que leq a bien le bon type.*)
Check leq.

(** Alternativement, on peut utiliser (comme en OCaml) le mot-clé [fun] 
à travers la structure:

     Definition f:= fun x y z => ...

pour définir des fonctions. La définition précédente est ainsi équivalente à:
*)

Definition leq':=fun n m => n-m = 0.



(** Comme vous le savez sûrement déjà, un ordre est une relation réflexive,
 anti-symétrique et transitive. Définissez ici ces différentes notions. *)

Definition reflexive (R:relation):= ... .
Definition antisymmetric (R:relation):= ... .
Definition transitive (R:relation):= ... .


Definition order (R:relation):= reflexive R /\ antisymmetric R /\ transitive R.


(** Dans la suite, si vous avez besoin de déplier une définition, vous
pouvez utiliser la tactique:
    [unfold f]
qui permet de remplacer dans le but le nom "f" définit plus haut par son 
expression. De même, vous pouvez déplier une opération dans l'une des 
hypothèses du contexte en utilisant:
    [unfold f in H]
*)


(** Pour commencer, on va montrer que l'ordre inversé (i.e. la relation 
"retournée") définit lui-même un ordre. 
Définissez ici ce qu'est le retournée d'une relation. *)

Definition reverse (R:relation):=.
Check reverse.




(** On peut maintenant montrer que si une relation est 
(réflexive|anti-symétrique|transitive) sa relation inverse l'est aussi. *)


Lemma rev_refl (R:relation): reflexive R -> reflexive (reverse R).
Qed.


Lemma rev_antisym (R:relation): antisymmetric R -> antisymmetric (reverse R).
Qed.

Lemma rev_trans (R:relation): transitive R -> transitive (reverse R).
Qed.


(** On peut maintenant facilement montrer qu'un ordre inversé est 
lui-même un ordre. *)

Lemma rev_order (R:relation):  order R -> order (reverse R).
Qed.





(*** Fonctions sur les listes ***)

(* Une "incantation" pour avoir accès à la bibliothèque Coq traitant des listes *)
Require Export List.

(* Coq connaît les listes polymorphes : les "listes de A", pour un type A quelconque
  (liste de naturels, de booléens, de listes de naturels, de fonctions de type nat -> nat, ..) *)
Print list.

(* Pour faciliter les choses, on va fixer ici un type de listes *)
Parameter A : Set.

(* Ainsi, quand vous déclarez un type de listes, utilisez "list A" *)

(* Définissez une fonction "rev" qui renvoie le retournement de son argument, 
en utilisant la fonction "++" qui permet de concatener deux listes, 
puis prouvez que c'est une involution, en s'aidant de ces lemmes intermédiaires. *)

Fixpoint rev l : list A := ...
(* Encore une fois, on peut utiliser le principe d'induction sur les listes [induction l] *)
Check list_ind.

(* On vous donne également quelques lemmes intérmédiaires en Coq *)
Check app_nil_end.
Check app_assoc.

(* Prouvez maintenant les lemmes suivants. Vous aurez besoin encore d'une nouvelle tactique :
    [rewrite H] 
    Si H est une hypothèse de la forme "a = b", [rewrite H] remplace dans le but toutes les 
    occurences de "a" par "b". Inversement, nous avons aussi:
    [rewrite <- H]
    Si H est une hypothèse de la forme "a = b", [rewrite H] remplace dans le but toutes les 
    occurences de "b" par "a". *)

Lemma rev_concat : forall xs ys, rev (xs ++ ys) = rev ys ++ rev xs.
Qed.

Lemma rev_singleton : forall xs x, rev (xs ++ (x :: nil)) = x :: rev xs.
Qed.

Lemma rev_involution : forall l, rev (rev l) = l.
Qed.

(* Ce n'est pas très économique de retourner des listes en appelant à
chaque fois "++". Trouver une solution plus rapide puis prouver
l'équivalence des deux. *)

Definition revopt l := ...


(* N'hésitez pas à prouver des lemmes intérmédiaires, surtout si vous utilisez des 
fonctions auxiliaires... *)

Lemma revopt_correct : forall l, revopt l = rev l.
Qed.


(* Voyez avec votre chargé de TP si vous avez programmé la version "optimisée" de rev. 
dans le cas contraire, introduisez rev' et montrez que les deux fonctions coïncident *)

(* Utiliser List.fold_left pour donner une définition alternative de la fonction qui 
  calcule la longueur d'une liste *)
Check List.fold_left.

Definition fold_length (l : list A) : nat := ...

(* Pour finir, définissez une fonction perms : nat -> list (list nat) telle que perms k 
  renvoie la liste de toutes les permutations de la liste [0;..;k] 
  Prouvez ensuite des tas de propriétés intéressantes de perms.
  Bonne Chance ! *)
