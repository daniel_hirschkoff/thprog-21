(********************* LOGIQUE 2 *************************)

(************************* BOOLEENS ************************)

 Inductive bool : Set :=
  | true
  | false.

Definition negb := fun b => 
  match b with
  | true => false
  | false => true
  end.

Definition andb := fun b1 b2 =>
  match b1 with
  | true => b2
  | false=> false
  end.

Example and_neg_ex : (negb (andb true false)) = true.
  reflexivity. Qed.

(* Avec les booléens, on peut faire if.. then.. else *)
Definition orb := fun (b1 b2:bool) =>
  if b1 then true else b2.

(* La définition suivante ne marche pas pour diverses raisons : *)
(* ("Fail" indique que l'on entre une définition erronée) *)
Fail Definition pred' := fun n =>
  if (exists k, n = S k) then k else 0.

Fail Definition egal2 := fun n => if n=2 then 100 else 0.







(* faux/absurde, dans Prop *)
Check False.

Lemma boum : forall P:Prop, False -> P.

intros. destruct H. Qed.

Lemma double_neg : forall P:Prop, P -> not (not P).

unfold not. intros. apply H0. trivial.
Qed.

(* on peut écrire ~ pour not (abréviation) *)
Lemma reciproquement : forall P:Prop, ~(~P) -> P.

unfold not. intros.
Abort.

Lemma tiers_exclu : forall P:Prop, P \/ ~P.
Abort.

Lemma nat_is_S : forall k, ~k=0 -> exists n, k = S n.
intro. destruct k. (* on pourrait faire induction k *)
  intro H. unfold not in H. destruct H. reflexivity.
  intros. exists k. reflexivity.
Qed.



Inductive le : nat -> nat -> Prop :=
| A : forall n, le 0 n
| R : forall n k, le n k -> le (S n) (S k).

Inductive odd : nat -> Prop :=
| Ao : odd 1
| Ro : forall k, odd k -> odd (S (S k)).


Lemma le_ex : forall n k, le n k -> 
exists x, k = n+x.

