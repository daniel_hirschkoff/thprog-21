



Lemma zero_left : forall n, 0+n = n.

reflexivity.
Qed.



Lemma zero_right : forall n, n+0 = n.


induction n.

(* Show 2. *)
  reflexivity.
  simpl. rewrite -> IHn. reflexivity.
Qed.





Print nat.



(* définition de fonction sur les nat *)
Definition pred (n : nat) : nat :=
  match n with    (* on "déconstruit" n *)
    | O => O
    | S n' => n'
  end.


(* addition entre naturels -- "+" est défini ainsi en Coq *)
Fixpoint plusnat (n m:nat) : nat := 
  match n with 
    | 0 => m  
    | (S k) => (S (plusnat k m)) 
  end.

Lemma plus_comm : forall n m, n+m = m+n.
induction n.
  intros. simpl.
  (* on utilise ici la tactique rewrite avec le lemme prouvé précédemment *)
  rewrite -> zero_right. reflexivity.
(* lire et comprendre  IHn *)
  intro. simpl.
(* regarder ce qui se passe si ci-dessous on met -> au lieu de <- *)
  rewrite <- plus_n_Sm. rewrite -> IHn. reflexivity.  Qed.



(* un peu plus de calculs avec les entiers naturels *)
Fixpoint half (n:nat) : nat :=
  match n with
  | 0 => 0
  | 1 => 0   (* ainsi 7/2 = 3 *)
  | S (S k) => S (half k)
  end.

(* somme des n premiers naturels *)
Fixpoint somme_n (n:nat) : nat :=
  match n with
  | 0 => 0
  | S k => (somme_n k) + (S k) 
  end.

Lemma add_S : forall n m, n=m -> S n = S m.
intros. rewrite -> H. reflexivity. Qed.

Lemma rid_S : forall n m, S n = S m -> n=m.

intros. 
inversion H. (* NB : ici, H1 a déjà été utilisée dans le but *)
reflexivity.
Qed.


Require Export Lia.   (* linear integer arithmetic *)
Require Export Arith.

Lemma half_twice_linear : forall n k, half (2*n + k) = n + half k.

induction n;intros.
simpl; reflexivity.
replace (2* S n+k) with (2*n + S (S k)).
rewrite -> IHn.
simpl.
auto.
lia.
Qed.



Lemma eq_somme_n : forall n, somme_n n = half (n * (n+1)).

induction n.
reflexivity.
simpl.
 replace (n + 1 + n * S (n + 1)) with (S(n + n * S (n + 1))) by lia.
replace (n + n * S (n + 1)) with (2*n+n*(n + 1)) by (rewrite Nat.mul_succ_r; lia).
rewrite half_twice_linear; lia.
Qed.







(****************** LOGIQUE : implication, conjonction, existentielle *****************)



(* -> pour l'implication *)
Lemma tautologie : forall A:Prop, A -> A.
(* trivial. *)
intros. trivial. Qed.


Lemma ratage : forall A B:Prop, A->B.

intros.
(* ??!?!? *)
Abort.


(* /\ pour la conjonction *)
Lemma modus_ponens : forall A B: Prop, (A /\ (A->B)) -> B.

intros. destruct H. apply H0. trivial. 
Qed.



Lemma and_comm : forall A B: Prop, A/\B -> B/\A.

intros. destruct H. split. trivial. trivial. 
Qed.




Lemma uncurry_curry : forall A B C : Prop,
  ((A /\ B) -> C)  ->  (A -> B -> C).

intros A B C.
intro H.
intro HA.
intro HB.

 apply H. split.
  trivial.  
  trivial.
Qed.

Lemma curry_uncurry : forall A B C : Prop,
  (A -> B -> C) -> ((A/\B) -> C).
intros. 
(* version maladroite *)
(* apply H. 
    destruct H0. trivial.
    destruct H0. trivial.
*)
destruct H0. apply H.  (* comprendre pourquoi ici 2 sous-buts sont engendrés *)
  trivial.
  trivial.
Qed.




(* une remarque en passant sur la tactique destruct *)
Lemma exemple : forall n, n-n = 0.

destruct n.
reflexivity.
(* NB : pas d'hypothèse d'induction ici *)
Abort.

Lemma toto : forall A:Prop, A/\A -> A.
intros A HA. destruct HA. trivial.
Qed.



(*** existentielle ***)


Lemma four_is_even : exists n : nat, 4 = n + n.
  exists 2. reflexivity. Qed.

(*
Utiliser une hypothèse exprimant une existentielle.
*)
Theorem exists_example_2 : forall n,
  (exists m, n = 4 + m) -> (exists o, n = 2 + o).
intros. destruct H. 
exists (2+x). rewrite -> H. reflexivity. Qed.





(************************** LISTES *************************)
(* Rappelons la définition des entiers naturels *)
Print nat.

Inductive natlist : Set :=
  nil : natlist 
| cons : nat -> natlist -> natlist.



(* NB : on aurait pu mettre "cons : (nat*natlist) -> natlist"
   en fait 
     nat*natlist -> natlist
     et
     nat -> natlist -> natlist
   sont équivalents : cf.  (A/\B) -> C  et  A -> B -> C  ci-dessus,
                      où * joue ici le rôle de /\.


   Le choix que l'on fait est bien plus pratique pour raisonner en Coq.
*)

Check natlist_ind.

(* définir une fonction sur les natlist *)
Fixpoint size (l:natlist) := match l with
| nil => O
| cons x xs => S (size xs) end.

Fixpoint append (l m:natlist) := match l with
| nil => m
| cons x xs => cons x (append xs m) end.

Lemma append_nil : forall l, append l nil = l.
induction l.
  reflexivity.
  simpl.  rewrite -> IHl. reflexivity.
Qed.


Check plus_n_Sm.




