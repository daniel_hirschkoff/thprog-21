(** TP 2 : Loqique, booléens, propositions et IMP **)

(** Correction des parties logique et Bool/Prop **) 

(*** Logique "avancée" et Coq : négation, booléens et Prop ***)

(* Négation *)
(* On peut voir le faux, noté False, comme un connecteur logique, qui
peut être inséré dans le "tableau" où figuraient la conjonction et
l'existentielle dans l'énoncé du TP 1 : 

    connecteur    | pour utiliser |  pour construire
  ----------------|---------------|---------------------
      P /\ Q      |  destruct H.  |  split.
      P \/ Q      |  destruct H.  |  left. / right.
  exists x:nat, P |  destruct H.  |  exists 17.
      False       |  destruct H.  |

Devinez :
- pourquoi il n'y a pas de tactique dans la colonne "pour construire" de ce tableau.
- ce que fait "destruct H." si H est une hypothèse qui dit False. *)


Lemma ex_falso: forall P : Prop, False -> P.
intros P H. destruct H. Qed.

(* Si l'on a A:Prop, alors ~A, la négation de A, est une notation pour "A -> False". *)

(* Si la notation vous perturbe, vous pouvez toujours utiliser la tactique [unfold not.] *)

Lemma not_not : forall P : Prop, P -> ~~P.
intros P H1 H2. apply H2. apply H1. Qed.

Lemma morgan1 : forall P Q : Prop, ~P \/ ~Q -> ~(P /\ Q).
intros P Q H0 H1. destruct H1. destruct H0.
  apply H0. apply H.
  apply H0. apply H1.
Qed.

Lemma morgan2 : forall P Q : Prop, ~P /\ ~Q -> ~(P \/ Q).
intros P Q H0 H1. destruct H0. destruct H1.
  apply H. apply H1.
  apply H0. apply H1.
Qed.


(* Pour la prochaine preuve, vous aurez besoin d'une nouvelle tactique : 
  [inversion H.]
    Formellement, lorsque l'on appelle [inversion H], Coq essaye de trouver les conditions nécessaires 
pour que l'hypothèse H soit vérifié. 
Coq fait aussi quelques simplifications lorsque l'on utilise inversion : si H est fausse, alors 
il termine automatiquement la preuve, si H implique une égalité entre 2 expressions, il fait 
parfois automatiquement le remplacement. 
Essayez cette tactique sur les prochains lemmes pour comprendre comment elle marche en pratique  *)

Lemma zero_un : ~(0=1).
intro H. inversion H. Qed.

(* Un autre exemple d'application de la tactique inversion, indépendamment de la négation *)
Lemma S_out :  forall n m : nat, S n = S m -> n = m.
intros n m H. inversion H. reflexivity. Qed.

(*** Les booléens et les propositions ***)

(* Il y a en Coq deux moyens d'exprimer des affirmations logiques, avec des booléens (le type bool) ou
des propositions (e type prop *)
Check True. 
Check False.
Check true.
Check false.

(*Un booléen est soit "true" soit "false". Ainsi, si on définit la fonction "and" suivante : *)

Definition Booland a b := match (a,b) with 
  | (true,true) => true
  | (true,false) => false
  | (false,true) => false
  | (false,false) => false
end.

(* Et qu'on l'évalue, on obtient soit true, soit false *)

Eval compute in (Booland false true).
Eval compute in (Booland true true).

(* En revanche, le "and" pour les propositions ne se réduit pas en "True" ou "False". Un objet de type "Prop" est 
intuitivement une expression booléennes, que l'on peut prouver, et sur laquelle on peut appliquer des tactiques *)
Eval compute in (False /\ True).

(* On va travailler un peu sur ces différences dans un exemple *) 
(* On donne deux moyens de prouver qu'un entier est pair. *)

Definition not a := match a with 
  |false => true
  |true => false
end.

Fixpoint evenb n := match n with 
  | 0 => true
  | S n' => not (evenb n')
end.

Definition even_bool n := evenb n = true.

(* Prouvez que 42 est pair avec cette définition *)

Lemma even_42_bool : even_bool 42.
unfold even_bool. simpl. reflexivity. Qed.

(* Un seconde définition avec une fonction "double" *)

Fixpoint double n := match n with 
  |0 => 0
  |S n' => S (S (double n'))
end.

Definition even_prop n := exists k, n = double k.

(* Prouvez une nouvelle fois que 42 est pair *)

Lemma even_42_prop : even_prop 42.
unfold even_prop. exists 21. simpl. reflexivity. Qed.

(* Et maintenant, on va prouvez que finalement, ces deux définitions sont équivalentes *)
(* On aura besoin pour cela de lemmes auxiliaires, prouvez les. *)
(* Indice : Vous pouvez utiliser la tactique [case a] quand "a" est un booléen pour traiter les deux cas "true" et "false" *)
(* Indice : N'oubliez pas d'utiliser [inversion] pour que Coq trouve automatiquement dans quel cas vous êtes dans la fonction 
evenb *)

Lemma Not_invol : forall a, not (not a) = a.
intro. case a. simpl. reflexivity. simpl. reflexivity. Qed.

Lemma Not_false : forall a, not a = false -> a = true.
intro. case a.
  intro. reflexivity.
  simpl. intro. inversion H.
Qed.

Lemma Not_true : forall a, not a = true -> a = false.
intro. case a.
  simpl. intro. inversion H.
  simpl. reflexivity.
Qed.

Lemma evenb_double : forall k, even_bool (double k).
intro k. unfold even_bool. induction k.
  simpl. reflexivity.
  simpl. rewrite Not_invol. apply IHk.
Qed.

(* On aura besoin de prouver un lemme plus fort que juste "evenb n = true -> exists k, n = double k" *)
(* Comprenez et completez la preuve à trous suivante: *)
Lemma evenb_double_conv : forall n, 
(evenb n = true -> exists k, n = double k) /\ (evenb n = false -> exists k, n = S (double k)).
induction n.
  split. intros. exists 0. simpl. reflexivity. intros. inversion H.
 simpl. destruct IHn. split.
    intros. destruct H0. apply Not_true. apply H1.
    exists (S x). rewrite H0. reflexivity.
    intros. destruct H. apply Not_false. apply H1.
    exists x. rewrite H. reflexivity.
Qed.

(* On peut maintenant prouvez l'équivalence entre les deux *)
Lemma even_bool_prop : forall n, even_prop n <-> even_bool n.
intros. unfold even_prop. unfold even_bool. split.
  intro. destruct H. rewrite H. apply evenb_double.
  intro. apply evenb_double_conv. apply H.
Qed.

(* Sur cet exemple, vous avez normalement constaté qu'il est plus difficile de travailler sur 
les preuves avec les booléens que les propositions.
En effet, on est passé assez facilement des propositions aux booléens (evenb_double) mais l'inverse était plus compliqué. *)


(* En revanche, sur la preuve que 42 est paire, sur les booléens il n'y a presque rien à faire, mais pour les propositions, 
il faut au minimum donner l'entier correspondant à l'existentiel... *)
(*Ou bien, si on ne veut pas donner l'entier, on peut faire la preuve suivante... *)

Lemma even_42_prop_bis : even_prop 42.
apply even_bool_prop. reflexivity. Qed.

(* Sur cet exemple, on ne gagne pas vraiment à utiliser cette preuve. Mais sur certains cas, il peut être utile de connaitre cette 
technique. Un exemple extreme serait la preuve en Coq du théorème des 4 couleurs, 
dans laquelle ce procédé est utilisé pour qu'une preuve qui aurait eu besoin d'une analyse de centaines de cas soit 
capturé par un calcul sur les booléens. *)

(* Un exemple plus simple serait le suivant. Prouvez ce lemme *)

Lemma not_even_1001 : ~(even_bool 1001).
unfold even_bool. simpl. intro H. inversion H. Qed.

(* Et celui-ci ? Voyez-vous le problème ?*)

(**Correction : Prouver le lemme directement reviens à prouver que pour tout k, double k <> 1001. Prouvez ça revient 
à prouver que tous les doubles sont pairs et que 1001 n'est pas pair... On est donc obligé d'utiliser 
le lemme even_bool_prop prouvé précedemment. **)

Lemma not_even_1001_bis : ~(even_prop 1001).
intro. apply not_even_1001. apply even_bool_prop. apply H. Qed. 


(* Maintenant que vous avez vu l'intêret des booléens, nous allons faire quelques exercices usuels 
sur les fonctions de ce type. *)

(*Prouvez le lemme suivant *)

Lemma andb_true_iff : forall a b, Booland a b = true <-> (a = true /\ b = true).
intros a b. split.
  case a. case b.
    intros. split. reflexivity. reflexivity.
    intros. inversion H.
  case b.
    intros. inversion H.
    intros. inversion H.
  intros. destruct H. rewrite H. rewrite H0. reflexivity.
Qed.

(* Définissez la fonction "Boolor" pour les booléens, puis prouvez le lemme suivant *)

Definition Boolor a b := match a,b with 
  | true,true => true 
  | true,false => true 
  | false,true => true 
  | false,false => false
end.

Lemma orb_true_iff : forall a b,
  Boolor a b = true <-> (a = true \/ b = true).
intros a b. split.
  case a.
    intros. left. reflexivity.
    case b.
      intros. right. reflexivity.
      intros. inversion H.
  intros. destruct H. 
    rewrite H. case b. reflexivity. reflexivity.
    rewrite H. case a. reflexivity. reflexivity.
Qed.













