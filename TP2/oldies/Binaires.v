


(*** Une représentation des entiers binaires en Coq ***)

(* On va s'intéresser à une façon de représenter les entiers binaires en Coq. Pour cela, on va définir les mots 
sur l'alphabet "0;1", puis on va se forcer à utiliser une unique représentation de chaque entier *)

Inductive word := 
|eps : word
|S0 : word -> word 
|S1 : word -> word.

(* Petite subtilité ici, on va représenter les entiers en commencant par le bit de poids FAIBLE. Ainsi, l'entier "4" sera 
représenté par le mot "001", c'est à dire "S0 (S0 (S1 eps))" en Coq *)

(* Définissez une fonction bin_to_int transformant un mot en l'entier Coq qu'il représente : *)

Fixpoint bin_to_int w := ...

Eval compute in (bin_to_int (S0 (S0 (S1 eps)))).

(* Ecrivez une fonction bin_succ qui donne le successeur d'un entier écrit en binaire *)

Fixpoint bin_succ w := ...

(* Prouvez alors le lemme suivant. Vous pouvez utiliser le lemme de Coq sur les entiers *)
Check plus_n_Sm.

Lemma bin_succ_correct : forall w, S (bin_to_int w) = bin_to_int (bin_succ w).
Qed.

(* On donne maintenant la fonction d'addition (très moche) suivante : *)

Fixpoint bin_add_aux w w' c := 
  match (w,w',c) with 
| (eps,eps,false) => eps
| (eps,eps,true) => S1 eps
| (eps,S0 x',false) => S0 x'
| (eps,S0 x',true) => S1 x'
| (eps,S1 x',false) => S1 x'
| (eps,S1 x',true) => bin_succ (S1 x')
| (S0 x,eps,false) => S0 x
| (S0 x,eps,true) => S1 x
| (S0 x,S0 x',false) => S0 (bin_add_aux x x' false)
| (S0 x,S0 x',true) => S1 (bin_add_aux x x' false)
| (S0 x,S1 x',false) => S1 (bin_add_aux x x' false)
| (S0 x,S1 x',true) => S0 (bin_add_aux x x' true)
| (S1 x,eps,false) => S1 x
| (S1 x,eps,true) => bin_succ (S1 x)
| (S1 x,S0 x',false) => S1 (bin_add_aux x x' false)
| (S1 x,S0 x',true) => S0 (bin_add_aux x x' true)
| (S1 x,S1 x',false) => S0 (bin_add_aux x x' true)
| (S1 x,S1 x',true) => S1 (bin_add_aux x x' true)
end.

Definition bin_add w w' := bin_add_aux w w' false.

(* Prouvez la correction de cette fonction *)
Require Import ZArith.
(* Pour cette fois, nous vous autorisons à utiliser la tactique [omega], permettant de résoudre la plupart des 
problèmes arithmétiques des entiers en Coq *)

Lemma bin_add_aux_correct : forall w w', (bin_to_int (bin_add_aux w w' false) = bin_to_int w + bin_to_int w')
/\ (bin_to_int (bin_add_aux w w' true) = 1 + bin_to_int w + bin_to_int w').
Qed.












