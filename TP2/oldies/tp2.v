(** TP 2 : Loqique, booléens, propositions et IMP **)

(*** Logique "avancée" et Coq : négation, booléens et Prop ***)

(* Négation *)
(* On peut voir le faux, noté False, comme un connecteur logique, qui
peut être inséré dans le "tableau" où figuraient la conjonction et
l'existentielle dans l'énoncé du TP 1 : 

    connecteur    | pour utiliser |  pour construire
  ----------------|---------------|---------------------
      P /\ Q      |  destruct H.  |  split.
      P \/ Q      |  destruct H.  |  left. / right.
  exists x:nat, P |  destruct H.  |  exists 17.
      False       |  destruct H.  |

Devinez :
- pourquoi il n'y a pas de tactique dans la colonne "pour construire" de ce tableau.
- ce que fait "destruct H." si H est une hypothèse qui dit False. *)

(* A vous de jouer : prouvez les lemmes suivants *)

Lemma ex_falso: forall P : Prop, False -> P.
Qed.

(* Si l'on a A:Prop, alors ~A, la négation de A, est une notation pour "A -> False". *)
(* Si la notation vous perturbe, vous pouvez toujours utiliser la tactique [unfold not.] *)

Lemma not_not : forall P : Prop, P -> ~~P.
Qed.

Lemma morgan1 : forall P Q : Prop, ~P \/ ~Q -> ~(P /\ Q).
Qed.

Lemma morgan2 : forall P Q : Prop, ~P /\ ~Q -> ~(P \/ Q).
Qed.


(* Pour la prochaine preuve, vous aurez besoin d'une nouvelle tactique : 
  [inversion H.]
    Formellement, lorsque l'on appelle [inversion H], Coq essaye de trouver les conditions nécessaires 
pour que l'hypothèse H soit vérifiée.
Coq fait aussi quelques simplifications lorsque l'on utilise inversion : si H est fausse, alors 
il termine automatiquement la preuve, si H implique une égalité entre 2 expressions, il fait 
parfois automatiquement le remplacement. 
Essayez cette tactique sur les prochains lemmes pour comprendre comment elle marche en pratique  *)

Lemma zero_un : ~(0=1).
Qed.
(*Notez que "x <> y" est un raccourci de "x = y -> False".*)

(* Un autre exemple d'application de la tactique inversion, indépendamment de la négation *)
Lemma S_out :  forall n m : nat, S n = S m -> n = m.
Qed.


(*Dans le même style, les différents constructeurs d'un type inductif ne peuvent pas renvoyer la même valeur.
Revenons sur la tentative de définition  des entiers avec le successeur ET le prédécesseur, vue en cours. *)
Inductive t : Set :=
  Ze : t | Pr : t -> t | Su : t -> t.

Lemma S_et_P : forall x y, Su x = Pr y -> False.
Qed.

(*** Les booléens et les propositions ***)

(* Il y a en Coq deux moyens d'exprimer des affirmations logiques, avec des booléens (le type bool) ou
des propositions (e type prop *)
Check True. 
Check False.
Check true.
Check false.

(*Un booléen est soit "true" soit "false". Ainsi, si on définit la fonction "and" suivante : *)

Definition Booland a b := match a,b with 
  | true,true => true
  | true,false => false
  | false,true => false
  | false,false => false
end.

(* Et qu'on l'évalue, on obtient soit true, soit false *)

Eval compute in (Booland false true).
Eval compute in (Booland true true).

(* En revanche, le "and" pour les propositions ne se réduit pas en "True" ou "False". Un objet de type "Prop" est 
intuitivement une expression booléenne, que l'on peut prouver, et sur laquelle on peut appliquer des tactiques *)
Eval compute in (False /\ True).

(* On va travailler un peu sur ces différences dans un exemple *) 
(* On donne deux moyens de prouver qu'un entier est pair. *)

Definition not a := match a with 
  |false => true
  |true => false
end.

Fixpoint evenb n := match n with 
  | 0 => true
  | S n' => not (evenb n')
end.

Definition even_bool n := evenb n = true.

(* Prouvez que 42 est pair avec cette définition *)

Lemma even_42_bool : even_bool 42.
Qed.

(* Un seconde définition avec une fonction "double" *)

Fixpoint double n := match n with 
  |0 => 0
  |S n' => S (S (double n'))
end.

Definition even_prop n := exists k, n = double k.

(* Prouvez une nouvelle fois que 42 est pair *)

Lemma even_42_prop : even_prop 42.
Qed.

(* Et maintenant, on va prouvez que finalement, ces deux définitions sont équivalentes *)
(* On aura besoin pour cela de lemmes auxiliaires, prouvez les. *)
(* Indice : Vous pouvez utiliser la tactique [case a] quand "a" est un booléen pour traiter les deux cas "true" et "false".
Cela ne modifiera que dans l'objectif.*)

Lemma Not_invol : forall a, not (not a) = a.
Qed.

Lemma Not_false : forall a, not a = false -> a = true.
Qed.

Lemma Not_true : forall a, not a = true -> a = false.
Qed.

Lemma evenb_double : forall k, even_bool (double k).
Qed.

(*Tentez de prouver que la définition booléenne implique la définition propositionnelle*)
Lemma even_bool_to_prop : forall n, even_bool n -> even_prop n.

Abort.

(* Dans certains cas, on aura besoin d'une hypothèse d'induction plus forte que ce l'on souhaite prouver.
Note : Comme l'hypothèse d'induction 'est' notre objectif, "intro H. induction x" donnera une hypothèse d'induction
plus faible que "induction x" puis "intro H" dans les sous-cas.*)
(* Comprenez et completez la preuve à trous suivante: *)
Lemma evenb_double_conv : forall n, 
(evenb n = true -> exists k, n = double k) /\ (evenb n = false -> exists k, n = S (double k)).
induction n.
  (* Traitez le cas n = 0 de l'induction *)

  (* Début du cas successeur : *)
    simpl. destruct IHn. split.
    intros. destruct H0. (* Premier cas du split à traiter. Si vous avez du mal à lire l'intérface Coq, n'hésitez pas à demander de l'aide *)
    intros. destruct H. (* Deuxième cas du split à traiter *)
Qed.

(* On peut maintenant prouver l'équivalence entre les deux *)
Lemma even_bool_prop : forall n, even_prop n <-> even_bool n.
Qed.

(* Sur cet exemple, vous avez normalement constaté qu'il est plus difficile de travailler sur 
les preuves avec les booléens que les propositions.
En effet, on est passé assez facilement des propositions aux booléens (evenb_double) mais l'inverse était plus compliqué. *)

(* En revanche, sur la preuve que 42 est pair, sur les booléens il n'y a presque rien à faire, mais pour les propositions, 
il faut au minimum donner l'entier correspondant à l'existentiel... *)
(* Ou bien, si on ne veut pas donner l'entier, on peut faire la preuve suivante... *)

Lemma even_42_prop_bis : even_prop 42.
apply even_bool_prop. reflexivity. Qed.

(* Sur cet exemple, on ne gagne pas vraiment à utiliser cette preuve. Mais sur certains cas, il peut être utile de connaitre cette 
technique. Un exemple extreme serait la preuve en Coq du théorème des 4 couleurs, 
dans laquelle ce procédé est utilisé pour qu'une preuve qui aurait eu besoin d'une analyse de centaines de cas soit 
capturé par un calcul sur les booléens. *)

(* Un exemple plus simple serait le suivant. Prouvez ce lemme *)

Lemma not_even_1001 : ~(even_bool 1001).
Qed.

(* Et celui-ci ? Voyez-vous le problème ?*)

Lemma not_even_1001_bis : ~(even_prop 1001).
Qed.


(* Petit bonus : La fin de cette partie permet de vous familiariser d'avantager aux équivalences bool/Prop.
Il n'est pas nécessaire de la faire en TP, particulièrement si vous avez l'impression d'être en retard.*)

(* Prouvez le lemme suivant *)

Lemma andb_true_iff : forall a b, Booland a b = true <-> (a = true /\ b = true).
Qed.

(* Définissez la fonction "Boolor" pour les booléens *)

(*Definition Boolor a b := ...*)

(* Prouvez comme ci-dessus l'équivalence avec \/ *)


(* Fin du bonus. *)

(**** Prédicats inductifs, expression arithmétiques et booléennes ****)

Require Import List.

(* On va définir des prédicats sur des list (nat * nat) que l'on voit comme un ensemble de couple adresse/valeur.
On peut voir cela comme une implémentation des map de C++ / dict de Python*)

(* Les valeurs sont des entiers. *)
Definition value := nat.
(* Les adresses seront implémentées par des entiers. *)
Definition addr := nat.
Definition dict := list (addr * value).

(* Inspirez vous de ce que vous avez vu en cours pour écrire le prédicat inductif (is_size l n) qui signifie que l est de taille n *)
Inductive is_size : dict -> nat -> Prop := ...

(* La tactique [inversion H] ne sert pas que dans les égalités, en fait, elle fonctionne aussi sur tous les prédicats inductifs.
Elle va vérifier quels cas sont possibles. Cela se voit dans le lemme suivant: *)
Lemma non_empty_size : forall p l n, is_size (p::l) n -> n <> 0.
Qed.

(* Définissez un prédicat (get l i x) qui signifie que (i,x) ∈ l *)
Inductive get : dict -> addr -> value -> Prop := ...

(* Le prédicat (update l i y l') signifie que l' est obtenu en 
   remplaçant la première occurence de (i,_) dans l' par (i,y). *)
(* On vous donne un des cas :

  ──────────────────────────────── head 
  update ((i,x)::l) i y ((i,y)::l)

*)
(* Ecrivez ce prédicat *)
Inductive update : dict -> addr -> value -> dict -> Prop := ...


(* Pour montrer une cohérence entre les deux prédicats, prouvez le lemme suivant *)
Lemma update_get : forall l' l i x, update l i x l' -> get l' i x.
Qed.


(* Les termes arithmétiques du langage sont donnés par
   la grammaire suivante *)
Inductive aexpr : Set :=
   | zero : aexpr
   | var : addr -> aexpr 
   | succ : aexpr -> aexpr
   | pred : aexpr -> aexpr.

(* Pour répresenter des variables, on va utiliser le "dict" pour définir la valeur d'une variable à une adresse. *)
(* Ainsi, on peut définir une relation "aeval" telle que "aeval l a x" soit vrai quand l'expression arithmétique "a" 
vaut la valeur "x" en utilisant "l" pour les variables *)
(* Vous aurez besoin de définir avant la fonction prédécesseur sur les entiers Coq. 
Ne l'appelez pas pred, c'est déjà pris dans aexpr.  *)

Inductive aeval : env -> aexpr -> value -> Prop := ...

(* Completez et prouvez le lemme suivant *)
(* Indice : utilisez la tactique [constructor] pour que Coq trouve automatiquement la règle inductive à appliquer *)
(* Utilisez la tactique [replace expr with expr'] pour que l'expression "expr" soit remplacé par "expr'". Coq vous 
demandera alors de prouvez que "expr = expr'" *)
Lemma aeval_example : aeval ((0,1)::nil) (succ (pred (pred (var 0)))) 1.
Qed.

(* On a aussi des expressions booléennes (<) *)
Inductive bexpr : Set :=
   | blt : aexpr -> aexpr -> bexpr.

(* De même, définissez une relation "beval" pour évaluer les booléens. *)
(* On utilisera la relation "<" de Coq, qui est, on le rapelle, de type "nat -> nat -> Prop" *)

Inductive beval : env -> bexpr -> bool -> Prop := ...

(* Prouvez le lemme suivant *)

(* Lorsque vous utilisez la tactique [apply] en Coq, il va parfois vous donnez un message d'erreur de la forme: 
"Error: Unable to find an instance for the variables v1, v2."
Dans ce cas là, ça veut dire que Coq n'arrive pas à trouver tout seul la valeur de ces inconnues dans son application. 
Vous pouvez alors utiliser la tactique [apply H with x1 x2], et ainsi, il considérera que l'inconnu "v1" vaut "x1", et 
que "v2" vaut "x2".
*)
(* Vous pouvez aussi utiliser la tactique [auto] pour que Coq prouve automatiquement des prédicats sur les entiers, par exemple :*)
Lemma auto_example : 3 <= 5.
auto. Qed.

Lemma beval_example : beval ((0,1)::((1,0)::nil)) (blt (succ (var 1)) (zero)) false.
Qed. 


(*** Une représentation des entiers binaires en Coq ***)

(* On va s'intéresser à une façon de représenter les entiers binaires en Coq. Pour cela, on va définir les mots 
sur l'alphabet "0;1", puis on va se forcer à utiliser une unique représentation de chaque entier *)

Inductive word := 
|eps : word
|S0 : word -> word 
|S1 : word -> word.

(* Petite subtilité ici, on va représenter les entiers en commencant par le bit de poids FAIBLE. Ainsi, l'entier "4" sera 
représenté par le mot "001", c'est à dire "S0 (S0 (S1 eps))" en Coq *)

(* Définissez une fonction bin_to_int transformant un mot en l'entier Coq qu'il représente : *)

Fixpoint bin_to_int w := ...

Eval compute in (bin_to_int (S0 (S0 (S1 eps)))).

(* Ecrivez une fonction bin_succ qui donne le successeur d'un entier écrit en binaire *)

Fixpoint bin_succ w := ...

(* Prouvez alors le lemme suivant. Vous pouvez utiliser le lemme de Coq sur les entiers *)
Check plus_n_Sm.

Lemma bin_succ_correct : forall w, S (bin_to_int w) = bin_to_int (bin_succ w).
Qed.

(* On donne maintenant la fonction d'addition (très moche) suivante : *)

Fixpoint bin_add_aux w w' c := 
  match (w,w',c) with 
| (eps,eps,false) => eps
| (eps,eps,true) => S1 eps
| (eps,S0 x',false) => S0 x'
| (eps,S0 x',true) => S1 x'
| (eps,S1 x',false) => S1 x'
| (eps,S1 x',true) => bin_succ (S1 x')
| (S0 x,eps,false) => S0 x
| (S0 x,eps,true) => S1 x
| (S0 x,S0 x',false) => S0 (bin_add_aux x x' false)
| (S0 x,S0 x',true) => S1 (bin_add_aux x x' false)
| (S0 x,S1 x',false) => S1 (bin_add_aux x x' false)
| (S0 x,S1 x',true) => S0 (bin_add_aux x x' true)
| (S1 x,eps,false) => S1 x
| (S1 x,eps,true) => bin_succ (S1 x)
| (S1 x,S0 x',false) => S1 (bin_add_aux x x' false)
| (S1 x,S0 x',true) => S0 (bin_add_aux x x' true)
| (S1 x,S1 x',false) => S0 (bin_add_aux x x' true)
| (S1 x,S1 x',true) => S1 (bin_add_aux x x' true)
end.

Definition bin_add w w' := bin_add_aux w w' false.

(* Prouvez la correction de cette fonction *)
Require Import ZArith.
(* Pour cette fois, nous vous autorisons à utiliser la tactique [omega], permettant de résoudre la plupart des 
problèmes arithmétiques des entiers en Coq *)

Lemma bin_add_aux_correct : forall w w', (bin_to_int (bin_add_aux w w' false) = bin_to_int w + bin_to_int w')
/\ (bin_to_int (bin_add_aux w w' true) = 1 + bin_to_int w + bin_to_int w').
Qed.












