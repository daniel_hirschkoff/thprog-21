(**** Formalisation de IMP (ou plutôt d'un "cousin" de IMP ****)

Require Import List.

(* Les valeurs calculées seront des entiers. *)
Definition value := nat.
(* Les addresses seront implémentées par des entiers. *)
Definition addr := nat.   
(* La mémoire sera représentée par une liste de couples adresse/valeur *)
Definition env := list (addr * value). 

(* Le prédicat (get l a x) signifie que (a,x) ∈ l. 
   Il est axiomatisé par les règles suivantes : 
                                    get l a x
  ────────────────── head  a≠b ──────────────────── tail
  get ((a,x)::l) a x            get ((b,y)::l) a x

*)

(* Inspirez vous de ce que vous avez vu en cours pour écrire ce prédicat *)
Inductive get : env -> addr -> value -> Prop := 
| get_head : forall l a x, get ((a,x)::l) a x
| get_tail : forall l a x b y, (a <> b) -> get l a x -> get ((b,y)::l) a x. 

(* Le prédicat (update l a y l') signifie que l' est obtenu en 
   remplaçant la première occurence de (a,_) dans l' par (a,y). *)
(* 
                                                       update l a x l'
  ──────────────────────────────── head  a≠b ────────────────────────────────── tail
  update ((a,x)::l) a y ((a,y)::l)            update ((b,y)::l) a x ((b,y)::l')

*)
(* Ecrivez ce prédicat *)
Inductive update : env -> addr -> value -> env -> Prop := 
| update_head : forall a x l y, update ((a,x)::l) a y ((a,y)::l)
| update_tail : forall b y l a x l', a <> b -> update ((b,y)::l) a x ((b,y)::l').

(* Les termes arithmétiques du langage sont donnés par
   la grammaire suivante *)
Inductive aexpr : Type :=
   | zero : aexpr
   | var : addr -> aexpr 
   | succ : aexpr -> aexpr
   | pred : aexpr -> aexpr. 

(* Définissez une relation "aeval" telle que "aeval s a x" soit vrai quand l'expression arithmétique "a" 
vaut la valeur "x" dans l'environnement mémoire "s" *)
(* Vous aurez certainement besoin de définir avant la fonction prédécesseur sur les entiers Coq *)

Definition nat_pred n := match n with 
  | 0 => 0
  | S k => k
end.

Inductive aeval : env -> aexpr -> value -> Prop :=
|aeval_zero : forall s, aeval s zero 0
|aeval_var : forall s ad v, get s ad v -> aeval s (var ad) v 
|aeval_succ : forall s a x, aeval s a x -> aeval s (succ a) (S x)
|aeval_pred : forall s a x, aeval s a x -> aeval s (pred a) (nat_pred x).

(* Completez et prouvez le lemme suivant *)
(* Indice : utilisez la tactique [constructor] pour que Coq trouve automatiquement la règle inductive à appliquer *)
(* Utilisez la tactique [replace expr with expr'] pour que l'expression "expr" soit remplacé par "expr'". Coq vous 
demandera alors de prouvez que "expr = expr'" *)
Lemma aeval_example : aeval ((0,1)::nil) (succ (pred (pred (var 0)))) 1.
constructor. replace 0 with (nat_pred 0). constructor. replace 0 with (nat_pred 1). constructor.
simpl. constructor. constructor. simpl. reflexivity. simpl. reflexivity. Qed.

(* On a aussi des expressions booléennes (<=)*)
Inductive bexpr : Type :=
   | blt : aexpr -> aexpr -> bexpr.

(* De même, définissez une relation "beval" pour évaluer les boolens. *)
(* On utilisera la relation "<" (ou ">=") de Coq, qui est, on le rapelle, de type "Nat -> Nat -> Prop" *)

Inductive beval : env -> bexpr -> bool -> Prop :=
|beval_lt_true : forall s a1 a2 v1 v2, aeval s a1 v1 -> aeval s a2 v2 -> v1 < v2 -> beval s (blt a1 a2) true 
|beval_lt_false : forall s a1 a2 v1 v2, aeval s a1 v1 -> aeval s a2 v2 -> v1 >= v2 -> beval s (blt a1 a2) false.

(* Prouvez le lemme suivant *)

(* Lorsque vous utilisez la tactique [apply] en Coq, il va parfois vous donnez un message d'erreur de la forme: 
"Error: Unable to find an instance for the variables v1, v2."
Dans ce cas là, ça veut dire que Coq n'arrive pas à trouver tout seul la valeur de ces inconnues dans son application. 
Vous pouvez alors utiliser la tactique [apply H with x1 x2], et ainsi, il considérera que l'inconnu "v1" vaut "x1", et 
que "v2" vaut "x2".
*)
(* Vous pouvez aussi utiliser la tactique [auto] pour que Coq prouve automatiquement des prédicats sur les entiers, par exemple :*)
Lemma auto_example : 3 <= 5.
auto. Qed.

Lemma beval_example : beval ((0,1)::((1,0)::nil)) (blt (succ (var 1)) (zero)) false.
apply beval_lt_false with 1 0. constructor. constructor. constructor. auto. constructor.
constructor. auto. Qed. 

(* Les programmes sont donnés par : *)
Inductive prg : Type :=
   | skip : prg
   | seq : prg -> prg -> prg (* Composition Séquentielle *)
   | ass : addr -> aexpr -> prg (*Assignement d'une valeur à une adresse *)
   | ifte : bexpr -> prg -> prg -> prg (* Conditionnelle *)
   | while : bexpr -> prg -> prg.

(* Ici on définit des notations pour nous simplifier la vie. Ne vous occupez pas trop de la syntaxe *)
Notation "# x" := (var x) (at level 0).
Notation "x << y" := (blt x y) (at level 70).
Notation "A ; B" := (seq A B) (at level 100, right associativity).
Notation "A <- B" := (ass A B) (at level 80, right associativity).

(* Voilà, le seul programme qui nous intéressera. *)
Definition pgm_add :=
     while (zero <<  #0)
       (0 <- pred #0; 
        1 <- succ #1).

(* sémantique opérationnelle à petits pas pour les configurations, qui sont des couples (prg,env) *)

(* Encore une fois, du blabla pour les notations *)
Reserved Notation "A ==> B" (at level 80). 

Definition state := (prg * env)%type.
Notation "[ π | p ]" := (π,p).

(* Completez la définition de la sémantique à petit pas suivante : *)
Inductive ss : state -> state -> Prop :=
  | ss_ass : forall s s' i a x, aeval s a x -> update s i x s' -> [i <- a | s] ==> [skip | s']
  | ss_seq_skip : forall s c, [skip ; c | s] ==> [ c | s ] 
  | ss_seq_seq : forall s s' c c' d, [c | s] ==> [c'|s'] -> [c ; d|s] ==> [ c' ; d | s']
  | ss_ifte_true : forall s b c d, beval s b true -> [ ifte b c d | s ] ==> [ c | s ]
  | ss_ifte_false : forall s b c d, beval s b false -> [ ifte b c d | s ] ==> [ d | s ]
  | ss_while : forall s b c, [ while b c | s ] ==> [ ifte b (c ; while b c) skip | s ]
where "A ==> B" := (ss A B). 

(* Maintenant, on va essayer d'utiliser cette sémantique sur notre exemple *) 

(* Completez le lemme suivant, qui correspond à une étape de sémantique à petit pas, et prouvez le *)
Lemma pgm_add_example : [ pgm_add | (0,1)::((1,1)::nil) ]  ==> 
[ ifte (zero << #0) ((0 <- pred #0; 1 <- succ #1) ; pgm_add) skip | (0,1)::((1,1)::nil)].
constructor. Qed.

(* En faisant les choses comme ça, on ne peut pas faire plusieurs étapes de sémantique et donc de montrer le résultat
final. Pour pouvoir le faire, on utilise la clôture reflexive et transitive de "==>" *)

Inductive ss_star : state -> state -> Prop := 
|ss_star_refl : forall s, ss_star s s
|ss_star_trans : forall s1 s2 s3, s1 ==> s2 -> ss_star s2 s3 -> ss_star s1 s3.

(* Prouvez maintenant le lemme suivant sur notre petit programme *)
(* Vous pouvez utiliser la tactique [repeat constructor] permettant à Coq d'utiliser la tactique [constructor]
tant que ça change le but *)
Lemma pgm_add_star : ss_star [ pgm_add | (0,1)::((1,1)::nil) ] [ skip | (0,0)::((1,2)::nil)].
apply ss_star_trans with ([ ifte (zero << #0) ((0 <- pred #0; 1 <- succ #1) ; pgm_add) skip | (0,1)::((1,1)::nil)]).
constructor.
apply ss_star_trans with ([ (0 <- pred #0; 1 <- succ #1) ; pgm_add| (0,1)::((1,1)::nil)]).
constructor.
apply beval_lt_true with 0 1.
repeat constructor.
repeat constructor.
auto.
apply ss_star_trans with ([ (skip ; 1 <- succ #1) ; pgm_add| (0,0)::((1,1)::nil)]).
repeat constructor.
apply ss_ass with 0.
replace 0 with (nat_pred 1).
repeat constructor.
simpl.
reflexivity.
repeat constructor.
apply ss_star_trans with ([ (1 <- succ #1) ; pgm_add| (0,0)::((1,1)::nil)]).
repeat constructor.
apply ss_star_trans with ([ skip ; pgm_add| (0,0)::((1,2)::nil)]).
repeat constructor.
apply ss_ass with 2.
repeat constructor.
auto.
repeat constructor.
auto.
apply ss_star_trans with ([ pgm_add| (0,0)::((1,2)::nil)]).
repeat constructor.
apply ss_star_trans with ([ ifte (zero << #0) ((0 <- pred #0; 1 <- succ #1) ; pgm_add) skip | (0,0)::((1,2)::nil)]).
repeat constructor.
apply ss_star_trans with ([ skip | (0,0)::((1,2)::nil)]).
repeat constructor.
apply beval_lt_false with 0 0.
repeat constructor.
repeat constructor.
auto.
constructor.
Qed.
(*** Une représentation des entiers binaires en Coq ***)

(* On va s'intéresser à une façon de représenter les entiers binaires en Coq. Pour cela, on va définir les mots 
sur l'alphabet "0;1", puis on va se forcer à utiliser une unique représentation de chaque entier *)

Inductive word := 
|eps : word
|S0 : word -> word 
|S1 : word -> word.

(* Petite subtilité ici, on va représenter les entiers en commencant par le bit de poids FAIBLE. Ainsi, l'entier "4" sera 
représenté par le mot "001", c'est à dire "S0 (S0 (S1 eps))" en Coq *)

(* Définissez une fonction bin_to_int transformant un mot en l'entier Coq qu'il représente : *)

Fixpoint bin_to_int w := match w with 
|eps => 0
|S0 x => 2*(bin_to_int x)
|S1 x => S (2* (bin_to_int x))
end.

Eval compute in (bin_to_int (S0 (S0 (S1 eps)))).

(* Ecrivez une fonction bin_succ qui donne le successeur d'un entier écrit en binaire *)

Fixpoint bin_succ w := match w with
|eps => S1 eps
|S0 x => S1 x
|S1 x => S0 (bin_succ x)
end.

(* Prouvez alors le lemme suivant. Vous pouvez utiliser le lemme de Coq sur les entiers *)
Check plus_n_Sm.

Lemma bin_succ_correct : forall w, S (bin_to_int w) = bin_to_int (bin_succ w).
intro. induction w.
simpl. reflexivity.
simpl. reflexivity.
simpl. rewrite <- IHw.
simpl. rewrite plus_n_Sm. reflexivity. Qed.

(* On donne maintenant la fonction d'addition (très moche) suivante : *)

Fixpoint bin_add_aux w w' c := 
  match (w,w',c) with 
| (eps,eps,false) => eps
| (eps,eps,true) => S1 eps
| (eps,S0 x',false) => S0 x'
| (eps,S0 x',true) => S1 x'
| (eps,S1 x',false) => S1 x'
| (eps,S1 x',true) => bin_succ (S1 x')
| (S0 x,eps,false) => S0 x
| (S0 x,eps,true) => S1 x
| (S0 x,S0 x',false) => S0 (bin_add_aux x x' false)
| (S0 x,S0 x',true) => S1 (bin_add_aux x x' false)
| (S0 x,S1 x',false) => S1 (bin_add_aux x x' false)
| (S0 x,S1 x',true) => S0 (bin_add_aux x x' true)
| (S1 x,eps,false) => S1 x
| (S1 x,eps,true) => bin_succ (S1 x)
| (S1 x,S0 x',false) => S1 (bin_add_aux x x' false)
| (S1 x,S0 x',true) => S0 (bin_add_aux x x' true)
| (S1 x,S1 x',false) => S0 (bin_add_aux x x' true)
| (S1 x,S1 x',true) => S1 (bin_add_aux x x' true)
end.

Definition bin_add w w' := bin_add_aux w w' false.

(* Prouvez la correction de cette fonction *)
(* Vous pouvez vous aider de la commande SearchAbout pour traiter les opérations entières *)
Require Import ZArith.
(* Pour cette fois, nous vous autorisons à utiliser la tactique [omega], permettant de résoudre la plupart des 
problèmes arithmétiques des entiers en Coq *)

Lemma bin_add_aux_correct : forall w w', (bin_to_int (bin_add_aux w w' false) = bin_to_int w + bin_to_int w')
/\ (bin_to_int (bin_add_aux w w' true) = 1 + bin_to_int w + bin_to_int w').
intro w. induction w.
  intro w'. case w'. 
    simpl. split. reflexivity. reflexivity.
    simpl. split. reflexivity. reflexivity.
    simpl. split. reflexivity. rewrite <- bin_succ_correct. omega. 
  intro w'. case w'.
    simpl. omega.
    intro w0. simpl. destruct (IHw w0). split. rewrite H. omega. rewrite H. omega.
    intro w0. simpl. destruct (IHw w0). split. rewrite H. omega. rewrite H0. omega.
  intro w'. case w'.
    simpl. split. omega. rewrite <- bin_succ_correct. omega.
    intro w0. simpl. destruct (IHw w0). split. rewrite H. omega. rewrite H0. omega.
    intro w0. simpl. destruct (IHw w0). split. rewrite H0. omega. rewrite H0. omega.
Qed.
