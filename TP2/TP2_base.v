(*** TP 2 : Loqique, booléens, propositions et IMP ***)


(** Note : si au bout d'une heure votre TP-man oublie de le dire, passez à la partie sur les listes !! *)


(** * Logique "avancée" et Coq : négation, booléens et Prop **)

(* Négation *)
(* On peut voir le faux, noté False, comme un connecteur logique, qui
peut être inséré dans le "tableau" où figuraient la conjonction et
l'existentielle dans l'énoncé du TP 1 : 

    connecteur    | pour utiliser |  pour construire
  ----------------|---------------|---------------------
      P /\ Q      |  destruct H.  |  split.
      P \/ Q      |  destruct H.  |  left. / right.
  exists x:nat, P |  destruct H.  |  exists 17.
      False       |  destruct H.  |

Devinez :
- pourquoi il n'y a pas de tactique dans la colonne "pour construire" de ce tableau.
- ce que fait "destruct H." si H est une hypothèse qui dit False. *)

(* A vous de jouer : prouvez les lemmes suivants *)

Lemma ex_falso: forall P : Prop, False -> P.
(* # *)  intros P H. destruct H. (* #*)
Qed.

(* Si l'on a A:Prop, alors ~A, la négation de A, est une notation pour "A -> False". *)
(* Si la notation vous perturbe, vous pouvez toujours utiliser la tactique [unfold not.] *)

Lemma not_not : forall P : Prop, P -> ~~P.
(* #*)  intros P H1 H2. apply H2. apply H1. (* #*)
Qed.

Lemma morgan1 : forall P Q : Prop, ~P \/ ~Q -> ~(P /\ Q).
(* #*)  intros P Q H0 H1. destruct H1. destruct H0.
  apply H0. apply H.
  apply H0. apply H1. (* #*)
Qed.

Lemma morgan2 : forall P Q : Prop, ~P /\ ~Q -> ~(P \/ Q).
(* #*)  intros P Q H0 H1. destruct H0. destruct H1.
  apply H. apply H1.
  apply H0. apply H1. (* #*)
Qed.


(* Pour la prochaine preuve, vous aurez besoin d'une nouvelle tactique : 
  [inversion H.]
Formellement, lorsque l'on appelle [inversion H], Coq essaye de 
trouver les conditions nécessaires pour que l'hypothèse H soit vérifiée.
Coq fait aussi quelques simplifications lorsque l'on utilise inversion : si H est fausse, alors 
il termine automatiquement la preuve, si H implique une égalité entre 2 expressions, il fait 
parfois automatiquement le remplacement. 
Essayez cette tactique sur les prochains lemmes pour comprendre comment elle marche en pratique  *)

Lemma zero_un : ~(0=1).
(* # *)  intro H. inversion H. (* # *)
Qed.
(*Notez que "x <> y" est un raccourci de "x = y -> False".*)

(* Un autre exemple d'application de la tactique inversion, indépendamment de la négation *)
Lemma S_out :  forall n m : nat, S n = S m -> n = m.
  (* # *) intros n m H. inversion H. reflexivity. (* # *)
Qed.


(* Dans le même style, les différents constructeurs d'un type inductif ne peuvent pas renvoyer la même valeur.
Revenons sur la tentative de définition des entiers avec le successeur ET le prédécesseur, évoquée en cours. *)
Inductive t : Set :=
  Ze : t | Pr : t -> t | Su : t -> t.

Lemma S_et_P : forall x y, Su x = Pr y -> False.
  (* # *) intros x y H. inversion H. (* # *)
Qed.

(** Food for thought : à méditer après ce TP, ou si par hasard vous avez fini 
le reste avant la fin. Avec la syntaxe au-dessus, il est évident que le 
"prédécesseur" du "successeur" de "zéro" et "zéro" ne définissent pas le même
 objet: *)

Lemma PSZ: Pr (Su Ze) = Ze -> False.
  intros H. inversion H.
Qed.

Require Export ZArith.

(** Sauriez-vous montrer la même chose en remplaçant "zéro" par un x (de type t)
quelconque ? 
On insiste sur le fait que cette question est plutôt pour emmener à la maison 
que pour pendant le TP. Nous serons par ailleurs ravis de recevoir des mails 
si vous avez des questions là-dessus.
Un indice : souvenez-vous de vos cours de maths, où vous avez sûrement été déjà 
amené.e.s à prouver des résultats plus forts et plus généraux pour parvenir à
vos fins. 
Si besoin, vous pourrez vous servir de la tactique [omega] (chargée avec la 
librairie ZArith au-dessus, pour résoudre de buts arithmétiques (notamment 
obtenir faux à partir d'une hypothèse évidemment fausse comme x < x ou 2 < 0), 
et vous pouvez aussi invoquer la tactique [Search bla] pour chercher des lemmes
contenant "bla", comme dans: *)

Search "<" "S".

(* $ *)

(** Plusieurs solutions ici, dans tous les cas on a besoin de renforcer 
le résultat. Une solution "minimale" consiste à prouver: *)

Lemma inj_aux : forall x, Pr (Su x) <> x /\ Su (Pr x) <> x.
intro x. induction x ; split ; intro H ; inversion H.
  destruct IHx. apply H2. assumption.
  destruct IHx. apply H0. assumption.
Qed.

(** d'où on tire facilement le résultat attendu.
Une solution plus générale consiste à observer que l'on a envie/besoin
de prouver le résultat par induction sur x, et que la bonne fondaison
de l'induction (i.e. le fait que l'induction marche) repose sur l'existence
de quelque chose qui diminue. En l'occurence, ce qui décroit ici c'est la 
"taille" de x, c'est-à-dire le nombre de constructeurs. 
On peut introduire cette mesure :*)

Fixpoint size t:=
  match t with
  |Ze => 0
  |Pr t => 1 + size t
  |Su t => 1 + size t
  end.

(** Et prouver le résultat général suivant, dont on tire facilement
le résultat attendu :*)

Lemma inj_size:
  forall x y, size x < size y -> x = y -> False.
Proof.
  intros x.
  induction x;intros y Size Eq.
  - rewrite Eq in Size. omega.
  - destruct y;try inversion Eq.
    simpl in Size.
    rewrite H0 in Size. omega.
  - destruct y;try inversion Eq.
    simpl in Size.
    rewrite H0 in Size. omega.
Qed.
      
(* $ *)

Lemma inj_PS: forall x, Pr (Su x) = x -> False.
(* # *)  intros x H. simpl in H. apply (inj_size x (Pr (Su x))).
  simpl. omega. rewrite H. reflexivity. (* # *)
Qed.




(** * Les booléens et les propositions **)

(* Il y a en Coq deux moyens d'exprimer des affirmations logiques, avec des booléens (le type bool) ou
des propositions (le type prop *)
Check True. 
Check False.
Check true.
Check false.

(*Un booléen est soit "true" soit "false". Ainsi, si on définit la fonction "and" suivante : *)

Definition Booland a b := match a,b with 
  | true,true => true
  | true,false => false
  | false,true => false
  | false,false => false
end.

(* Et qu'on l'évalue, on obtient soit true, soit false *)

Eval compute in (Booland false true).
Eval compute in (Booland true true).


(* Il est important de garder à l'esprit que ceci est spécifiqueau type "bool".
En effet, un objet de type "Prop" n'est pas quelque chose que l'on peut 
simplifier soit en True soit en False, mais plutôt un énoncé dont on peut 
avoir une preuve (preuve que l'on peut construire en Coq à l'aide de tactiques).
*)

(* # Ancienne version: 
En revanche, le "and" pour les propositions ne se réduit pas en "True" ou
 "False". Un objet de type "Prop" est intuitivement une expression booléenne, 
que l'on peut prouver, et sur laquelle on peut appliquer des tactiques *)

Eval compute in (False /\ True).

(* On va travailler un peu sur ces différences dans un exemple *) 
(* On donne deux moyens de prouver qu'un entier est pair. *)

Definition not a := match a with 
  |false => true
  |true => false
end.

Fixpoint evenb n := match n with 
  | 0 => true
  | S n' => not (evenb n')
end.

Definition even_bool n := evenb n = true.

(* Prouvez que 42 est pair avec cette définition *)

Lemma even_42_bool : even_bool 42.
  (* # *) unfold even_bool. simpl. reflexivity. (* # *)
Qed.

(* Une seconde définition avec une fonction "double" *)

Fixpoint double n := match n with 
  |0 => 0
  |S n' => S (S (double n'))
end.

Definition even_prop n := exists k, n = double k.

(* Prouvez une nouvelle fois que 42 est pair *)

Lemma even_42_prop : even_prop 42.
(* # *) unfold even_prop. exists 21. simpl. reflexivity. (* # *)
Qed.

(* Et maintenant, on va prouvez que finalement, ces deux définitions sont équivalentes *)
(* On aura besoin pour cela de lemmes auxiliaires, prouvez les. *)
(* Indice : Vous pouvez utiliser la tactique [case a] quand "a" est un booléen pour traiter les deux cas "true" et "false".
Cela ne modifiera que dans l'objectif.*)

Lemma Not_invol : forall a, not (not a) = a.
 (* # *) intro. case a. simpl. reflexivity. simpl. reflexivity. (* # *)
Qed.

Lemma Not_false : forall a, not a = false -> a = true.
 (* # *)  intro. case a.
  intro. reflexivity.
  simpl. intro. inversion H.  (* # *)
Qed.

Lemma Not_true : forall a, not a = true -> a = false. 
 (* # *) intro. case a.
  simpl. intro. inversion H.
  simpl. reflexivity.  (* # *)
Qed.

Lemma evenb_double : forall k, even_bool (double k).
 (* # *) intro k. unfold even_bool. induction k.
  simpl. reflexivity.
  simpl. rewrite Not_invol. apply IHk.  (* # *)

Qed.

(*Tentez de prouver que la définition booléenne implique la définition propositionnelle*)
Lemma even_bool_to_prop : forall n, even_bool n -> even_prop n.

Abort.

(* Dans certains cas, on aura besoin d'une hypothèse d'induction plus forte que ce l'on souhaite prouver.
Note : Comme l'hypothèse d'induction 'est' notre objectif, "intro H. induction x" donnera une hypothèse d'induction
plus faible que "induction x" puis "intro H" dans les sous-cas.*)
(* Comprenez et completez la preuve à trous suivante: *)

Lemma evenb_double_conv : forall n, 
(evenb n = true -> exists k, n = double k) /\ (evenb n = false -> exists k, n = S (double k)).
induction n.
  (* Traitez le cas n = 0 de l'induction *)
(* # *) split. intros. exists 0. simpl. reflexivity. intros. inversion H. (* # *)
  (* Début du cas successeur : *)
    simpl. destruct IHn. split.
    intros. destruct H0. (* # *) apply Not_true. apply H1.
    exists (S x). rewrite H0. reflexivity. (* # *) (* Premier cas du split à traiter. Si vous avez du mal à lire l'intérface Coq, n'hésitez pas à demander de l'aide *)
    intros. destruct H. (* # *)  apply Not_false. apply H1.
    exists x. rewrite H. reflexivity. (* # *) (* Deuxième cas du split à traiter *)
Qed.

(* On peut maintenant prouver l'équivalence entre les deux *)
Lemma even_bool_prop : forall n, even_prop n <-> even_bool n.
  (* # *) intros. unfold even_prop. unfold even_bool. split.
  intro. destruct H. rewrite H. apply evenb_double.
  intro. apply evenb_double_conv. apply H. (* # *)
Qed.

(* Sur cet exemple, vous avez normalement constaté qu'il est plus difficile de travailler sur 
les preuves avec les booléens que les propositions.
En effet, on est passé assez facilement des propositions aux booléens (evenb_double) mais l'inverse était plus compliqué. *)

(* En revanche, sur la preuve que 42 est paire, sur les booléens il n'y a presque rien à faire, mais pour les propositions, 
il faut au minimum donner l'entier correspondant à l'existentiel... *)
(* Ou bien, si on ne veut pas donner l'entier, on peut faire la preuve suivante... *)

Lemma even_42_prop_bis : even_prop 42.
apply even_bool_prop. reflexivity. Qed.

(* Sur cet exemple, on ne gagne pas vraiment à utiliser cette preuve. Mais sur certains cas, il peut être utile de connaitre cette 
technique. Un exemple extreme serait la preuve en Coq du théorème des 4 couleurs, 
dans laquelle ce procédé est utilisé pour qu'une preuve qui aurait eu besoin d'une analyse de centaines de cas soit 
capturé par un calcul sur les booléens. *)

(* Un exemple plus simple serait le suivant. Prouvez ce lemme *)

Lemma not_even_1001 : ~(even_bool 1001).
(* # *)  unfold even_bool. simpl. intro H. inversion H. (* # *)
Qed.

(* Et celui-ci ? Voyez-vous le problème ?*)

Lemma not_even_1001_bis : ~(even_prop 1001).
(* # *)  intro. apply not_even_1001. apply even_bool_prop. apply H. (* # *)
Qed.

(* $$ *)
(** Correction : Prouver le lemme directement reviens à prouver que pour tout k, double k <> 1001. Prouvez ça revient 
à prouver que tous les doubles sont pairs et que 1001 n'est pas pair... On est donc obligé d'utiliser 
le lemme even_bool_prop prouvé précedemment. **)
(* $$ *)

(* Petit bonus : La fin de cette partie permet de vous familiariser d'avantager aux équivalences bool/Prop.
Il n'est pas nécessaire de la faire en TP, particulièrement si vous avez l'impression d'être en retard.*)
(* Si Coq râle, n'hésitez à commenter les parties non faites *)

(* Prouvez le lemme suivant *)

Lemma andb_true_iff : forall a b, Booland a b = true <-> (a = true /\ b = true).
  (* # *)
  intros a b. split.
  case a. case b.
    intros. split. reflexivity. reflexivity.
    intros. inversion H.
  case b.
    intros. inversion H.
    intros. inversion H.
  intros. destruct H. rewrite H. rewrite H0. reflexivity.
  (* # *)
Qed.

(* Définissez la fonction "Boolor" pour les booléens *)

Definition Boolor a b := (* # *)match a,b with 
  | true,true => true 
  | true,false => true 
  | false,true => true 
  | false,false => false
end(* # *).

(* Prouvez comme ci-dessus l'équivalence avec \/ *)

(* ## *)
Lemma orb_true_iff : forall a b,
  Boolor a b = true <-> (a = true \/ b = true).
intros a b. split.
  case a.
    intros. left. reflexivity.
    case b.
      intros. right. reflexivity.
      intros. inversion H.
  intros. destruct H. 
    rewrite H. case b. reflexivity. reflexivity.
    rewrite H. case a. reflexivity. reflexivity.
Qed.
(* ## *)

(* Fin du bonus. *)




(** * Prédicats inductifs, listes et relations **)

Require Import List.

(* On va définir des prédicats sur des listes. Pour simplifier, on supposera qu'il s'agit de listes d'entiers.*)

Definition listn := list nat.

(* Inspirez vous de ce que vous avez vu en cours pour écrire le prédicat inductif (is_size l n) qui signifie que l est de taille n *)
Inductive is_size : listn -> nat -> Prop := 
(* ## *)
| size_nil : is_size nil 0
| size_cons : forall x l n, is_size l n -> is_size (x::l) (S n).
(* ## *)

(* La tactique [inversion H] ne sert pas que dans les égalités, en fait, elle fonctionne aussi sur tous les prédicats inductifs.
Elle va vérifier quels cas sont possibles. Cela se voit dans le lemme suivant: *)

Lemma non_empty_size : forall p l n, is_size (p::l) n -> n <> 0.
  (* # *)
  intros p l n H Hn.
  rewrite Hn in H.
  inversion H.
  (* # *)
Qed.

(* Définissez un prédicat (mem l x) qui signifie que x ∈ l *)
Inductive mem : listn -> nat -> Prop := 
(* ## *)
| mem_head : forall x l, mem (x::l) x
| mem_tail : forall x l, mem l x -> forall y, mem (y::l) x.
(* ## *)

(* Le prédicat (update l x y l') signifie que l' est obtenu en 
   remplaçant la première occurence de x dans l' par y. *)

(* On vous donne un des cas :

  ──────────────────────────────── head 
  update (x::l) x y (y::l)

*)

(* Ecrivez ce prédicat *)
Inductive update : listn -> nat -> nat -> listn -> Prop := 
(* ## *)
| update_head : forall x y l, update (x::l) x y (y::l)
| update_tail : forall x y l l', update l x y l' -> forall z, x <> z -> update (z::l) x y (z::l').
(* ## *)


(* Pour montrer une cohérence entre les deux prédicats, prouvez le lemme suivant *)
Lemma update_mem : forall l x y l', update l x y l' -> mem l x.
(* ## *)
intros l x y. induction l. (* Il ne faut pas introduire l' pour avoir une hypothèse d'induction correcte *)
  intros. inversion H.
  intros. inversion H ; subst.
    apply mem_head.
    apply mem_tail. apply IHl with l'0. assumption.
(* ## *)
Qed.

(* On pourrait prouver de manière similaire "forall l' l x y, update l x y l' -> mem l' y." *)

(* Petit bonus *)
(* Pour prouver une implication dans l'autre sens, vous pourrez avoir besoin du lemme suivant: *)
Lemma eq_dec : forall n m : nat, n = m \/ n <> m.
(* # *)
intro n. induction n.
  intro m. case m.
    left. reflexivity.
    right. intro H. inversion H.
  intro m. case m.
    right. intro H. inversion H.
    intro n0. destruct IHn with n0.
      left. rewrite H. reflexivity.
      right. intro H0. inversion H0. apply H. apply H2.
(* # *)
Qed.

Lemma mem_update : forall l x, mem l x -> forall y, exists l', update l x y l'.
(* # *)
intros. induction l.
  inversion H.
  inversion H ; subst.
    exists (y :: l). constructor.
    destruct eq_dec with x a.
      exists (y::l). rewrite H0. constructor.
      destruct IHl.
        assumption.
        exists (a :: x0). constructor.
          assumption.
          assumption.
(* # *)
Qed.

(* P.S : Si vous avez réussi à le prouver sans utiliser le lemme au-dessus, 
votre définition de mem aurait probablement pu être plus simple. *)

(* Fin du petit bonus *)

(* On considère les listes et on définit une relation binaire inductive sur les listes. On note 
"perm l1 l2" cette relation, avec l'idée que "perm l1 l2" représente le fait que l2 est une 
permutation de l1.
  Cette définition inductive est donnée par les quatres règles d'inférence suivantes : 

                                       perm l₁ l₂      perm l₂ l₃
  ───────────── (refl)                 ──────────────────────────── (trans) 
    perm l l                                   perm l₁ l₃


        perm l₁ l₂
────────────────────── (tail)          ────────────────────────── (head) 
  perm (x::l₁) (x::l₂)                  perm (x::y::l) (y::x::l)


*)

Inductive perm  : (* # *)listn -> listn -> Prop (* # *):=
(* ## *)
| perm_refl : forall l, perm l l
| perm_trans : forall l1 l2 l3, perm l1 l2 -> perm l2 l3 -> perm l1 l3
| perm_tail : forall l1 l2, perm l1 l2 -> forall x, perm (x::l1) (x::l2)
| perm_head : forall x y l, perm (x::y::l) (y::x::l)
(* ## *).

(* Petit bonus *)
(* Vous vous êtes peut être demandé pourquoi est-ce que l'on a pris cette règle-là pour head et pas une autre ? 
  Notamment, on aurait pu penser à prendre cette règle-ci : 

           perm l₁ l₂
   ─────────────────────────────(head')
     perm (x::y::l₁) (y::x::l₂) 

On peut cependant prouver que l'on a pas besoin de cette règle. 
Pour vous aider à comprendre pourquoi, montrez d'abord l'exemple suivant. *)

Lemma perm_example : perm (1::2::4::5::3::nil) (2::1::4::3::5::nil).
(* ## *)
apply perm_trans with (1::2::4::3::5::nil).
  constructor. constructor. constructor. constructor.
  constructor.
(* ## *)
Qed.

(* Prouvez maintenant le lemme suivant, montrant que la règle donnée
 précédemment est vraie dans notre définition. *)

Lemma perm_head_alt : forall x y l1 l2, perm l1 l2 -> perm (x::y::l1) (y::x::l2).
(* ## *)
intros. inversion H ; subst.
  constructor.
  apply perm_trans with (x::y::l2).
    constructor. constructor. assumption.
    constructor.
  apply perm_trans with (x::y::x0::l3).
    constructor. constructor. constructor. assumption.
    constructor.
  apply perm_trans with (x::y::y0::x0::l).
    constructor. constructor. constructor.
    constructor.
(* ## *)
Qed.

(* Fin du petit bonus *)



Parameter X : Set.
Definition relation:= X -> X -> Prop.

(* Définir le prédicat inductif union R1 R2 qui est l'union de deux relations *)
Inductive union : relation -> relation -> X -> X -> Prop :=
(* ## *)
| union_left : forall (R1 : relation) x y, R1 x y -> forall R2, union R1 R2 x y
| union_right : forall (R2 : relation) x y, R2 x y -> forall R1, union R1 R2 x y.
(* ## *)

(* De même pour l'intersection *)
Inductive inter : relation -> relation -> relation :=
(* ## *)
| inter_base : forall (R1 R2 : relation) x y, R1 x y -> R2 x y -> inter R1 R2 x y.
(* ## *)

(* Pour comparer des relations entre elles, il faut définir la notion d'égalité, '=' est trop fort ici.*)
Definition equal : relation -> relation -> Prop := (* # *) fun R1 R2 => forall x y, R1 x y <-> R2 x y. (* # *)

(* On peut maintenant prouver la distributivité entre les deux par exemple *)
Lemma distr : forall R1 R2 R3, equal (inter (union R1 R2) R3) (union (inter R1 R3) (inter R2 R3)).
(* ## *)
intros R1 R2 R3 x y. split.
  intro H. inversion H ; subst. inversion H0 ; subst.
    apply union_left. apply inter_base ; assumption.
    apply union_right. apply inter_base ; assumption.
  intro H. inversion H ; subst.
    inversion H0 ; subst.
      apply inter_base.
        apply union_left. assumption.
        assumption.
    inversion H0 ; subst.
      apply inter_base.
        apply union_right. assumption.
        assumption.
(* ## *)
Qed.
