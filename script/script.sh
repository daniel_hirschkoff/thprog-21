#!/bin/bash


# Crée DM1.v et DM1_correction à partir du fichier DM1_base.v.
#
# Pour le bureau des plaintes, s'adresser à etienne.miquey@ens-lyon.fr


# On passe le nom du fichier à produire en argument, par défault on prend
# le même que celui du répertoire

if test -z "$1"
then   
    name=${PWD##*/}
else
    name="$1"
fi
base="$name"_base.v
output="$name".v
corr="$name"_correction.v



# Comportement :
# 1) Génère les trous en remplaçant tout ce qui se trouve entre
# deux commentaires de la forme (* # *) par [...]
# (insensibles normalement aux espaces et nombre de dièses)
#   ex: 
#     - intro H. (* # *) apply H. (* # *)  Qed.
#
#        ==devient==>
#
#       intro H. [...] Qed.
#
#
#      - intro H.
#        (* # *)
#         apply H.
#        (* # *)
#        Qed.
#
#        ==devient==>
#
#       intro H.
#       [...]
#       Qed.
#
# 2) Même chose avec $ au lieu de # mais sans substitution par [...].
#
# 3) Supprime les commentaires de la formes (* # ....... *), pour
# permettre aux personnes incroyables qui écrivent les sujets de
# "discuter" dans le fichier _base.v
#
# 4) Nettoie ces commentaires / marqueurs pour créer le corrigé
#
#
# Voir aussi le dossier test qui contient un exemple.



mk_blank1="s/(\*[ \s\t]*\$*[ \s\t]*\*).*(\*[ \s\t]*\$*[ \s\t]*\*)//g"
mk_blank="/(\*[ \s\t]*\$*[ \s\t]*\*)/{:boucle;N;s/(\*[ \s\t]*\$*[ \s\t]*\*).*(\*[ \s\t]*\$*[ \s\t]*\*)//;T boucle}"
mk_hole1="s/(\*[ \s\t]*#*[ \s\t]*\*)[^#]*(\*[ \s\t]*#*[ \s\t]*\*)/[...]/g"
mk_hole="/(\*[ \s\t]*#*[ \s\t]*\*)/{:boucle;N;s/(\*[ \s\t]*#*[ \s\t]*\*).*(\*[ \s\t]*#*[ \s\t]*\*)/[...]/;T boucle}"
rm_markers2="s/(\*[ \s\t]*[#\$]*[ \s\t]*\*)//g"
rm_markers="/^[ \s\t]*(\*\s*[#\$]*\s*\*)[ \s\t]*$/d"
rm_comm="/(\*[ \s\t]*[#\$]/{:boucle /\*)/! {N;b boucle}; /\*)/{b fin};:fin s/(\*[ \s\t]*[#\$].*\*)//;/^[ \s\t]*$/d}"


# Sujet
sed "$mk_hole1" $base | sed "$mk_hole"| sed "$mk_blank1" |sed "$mk_blank"| sed "$rm_comm" > $output


# Corrigé
sed "$rm_markers" $base | sed "$rm_markers2" | sed "$rm_comm" > $corr


echo "Les fichiers $output et $corr ont été créés."
