(* Ceci est un commentaire normal qui va rester. *)

Lemma trivial1 : forall P : Prop, P -> P.
intro P. intro H. (* # *) (* # hop on crée le trou ici*) apply H (* # *). Qed.


Lemma trivial2 : forall P Q : Prop, (P -> Q) -> P -> Q.
  intro P.
  intro Q. (* # *)
  intro H1. intro H2.
  apply H1. (* # *) apply H2.
(* # Ici on pourrait dire : mais quel bel exercice ! 
Cela n'apparaîtra ni dans le sujet ni dans le corrigé *)
Qed.

(* $ *)
(** Ici un commentaire qui n'apparaîtra qu'à la correction *)
(* $ *)


Lemma trivial3: (* # *) forall P, P -> P (* # *).
  (* $ *) intro H. apply H. (* $ *)  Qed.

