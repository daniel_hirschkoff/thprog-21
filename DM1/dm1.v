
(* L3 IF, 2021-2022, cours Théorie de la Programmation, DM 1.
   À rendre en déposant le fichier nommé DM1-nomdefamille.v sur le portail des études
   pour le 5 octobre à 23h59 au plus tard.

  À faire seul(e). Vous pouvez solliciter les chargés de TP sur
  discord (ou par mail) si vous avez des questions.


On s’intéresse ici à une représentation d’une liste à l’aide de deux listes. 
Pour simplifier, on ne considérera que des listes d’entiers naturels (list nat) en Coq.


L’intuition est que l’on “pointe” un endroit dans la liste (entre deux entiers), 
avec d’un côté ce qui est avant, et de l’autre ce qui est après. 
On convient que le couple (l1,l2) représente la liste commençant par la liste l1 
à l’envers, suivie de la liste l2. Par exemple, le couple ([2;4], [5]) représente 
la liste [4;2;5], le pointeur étant entre le 2 et le 5. 
Les couples ([2], [4;5]), ([], [2;4;5]) et ([5;4;2],[]) représentent également la même liste.
Ce fichier sert de trame à votre devoir : il s’agit de le renvoyer rempli.

Dans les solutions que vous proposez, il faut s’astreindre à ne pas parcourir inutilement 
les listes, avec l’idée qu’ajouter un élément à une liste (opérateur ::) a un coût, 
et déconstruire une liste (récupérer la tête et la queue de la liste, si celle-ci n’est pas 
vide) a également un coût.
Comme évoqué ci-dessus, une liste d’entiers peut avoir plusieurs représentations équivalentes. 
Lorsqu’on a le choix, on privilégiera la représentation avec “un maximum d’éléments 
à droite” (intuitivement parce que la liste de droite est celle qui est à l’endroit), 
mais on ne va pas s’amuser à parcourir des bouts de liste uniquement pour en faire 
basculer certains à droite.
*)

(************************************************************************************)

(* Nous faisons appel à la librairie sur les listes. 
   Commencez par lire la documentation sur cette librairie, certains des
   résultats qui y sont prouvés pourront vous être utiles :
   https://coq.inria.fr/library/Coq.Lists.List.html
*)
Require Export List.


(* On introduit des incantations récupérées du livre "Software Foundations", 
   de B. Pierce et al., pour utiliser les notations habituelles avec les crochets *)
Notation "x :: l" := (cons x l)
                     (at level 60, right associativity).
Notation "[ ]" := nil.
Notation "[ x ; .. ; y ]" := (cons x .. (cons y nil) ..).


(* La représentation des listes sur laquelle on travaille pour ce DM : *)
Definition dl  : Set := (list nat)*(list nat).

Definition sizetwo := fun d : dl => length (fst d) + length (snd d).


(* Des listes de nat aux dl *)
Definition init : (list nat)-> dl := fun l => ([],l).


(* Des dl aux listes de nat.
   Définissez t2o "directement", sans faire appel à
   des fonctions définies dans la librairie List. *)
Fixpoint t2o (l1 l2: list nat) := (* # *) match l1 with
| [] => l2
| x::xs => t2o xs (x::l2) end (* # *).


(* Utilisez t2o pour définir la fonction suivante, qui est l'"inverse"
   de init. *)
Definition two2one : dl -> list nat := (* # *)fun d => match d with 
| (l1,l2) => t2o l1 l2 end (* # *).




(*******************************************************************************)

Require Export Arith. (* pour connaître des lemmes ci-dessous *)

(* On introduit la fonction suivante *)
Fixpoint compare_lists l1 l2 := 
  match l1,l2 with
| nil,nil => true
| x::xs, y::ys => if Nat.eq_dec x y then compare_lists xs ys else false
| _,_ => false end.

(* La fonction compare_lists fait un if-then-else, en se servant de Nat.eq_dec : *)
Check Nat.eq_dec.
(* Nat.eq_dec
     : forall n m : nat, {n = m} + {n <> m} *)
(* La notation { .. } + { .. } correspond à une "disjonction dans Set", sur
   laquelle on peut faire un if-then-else. Cette notation s'appuie sur le type
   sumbool de Coq. 
   Vous pouvez par exemple utiliser la commande *)
Search sumbool lt.
(* pour voir l'ensemble des lemmes que connaît Coq faisant intervenir lt et sumbool. *)





(* transitivité *)
(* Pour prouver ce lemme, vous pouvez utiliser une seule induction, et pas mal
   de "destruct". À noter que destruct peut être utilisé sur une liste, mais on
   peut aussi faire destruct (Nat.eq_dec k k'), où k et k' sont des nat. *)
Lemma compare_lists_t : forall l1 l2 l3, 
  compare_lists l1 l2 = true -> compare_lists l2 l3 = true ->
  compare_lists l1 l3 = true.
(* # *)
induction l1.
(* on nettoie... *)
destruct l2. destruct l3.
simpl;trivial.
simpl; intros. inversion H0.
simpl; intros. inversion H.
destruct l2. intros l3 H; inversion H.
destruct l3. intros H0 H; inversion H.

(* ..le "vrai" début de la preuve pour le cas inductif *)
simpl.
destruct (Nat.eq_dec a n); intro.  (* DH : est-il difficile de deviner ce destruct ? *)
rewrite e; destruct (Nat.eq_dec n n0); intro.
apply IHl1 with l2; trivial.
inversion H0.
inversion H.
(* # *)
Qed.


(* symétrie *)
Lemma compare_lists_s : forall l1 l2, 
   compare_lists l1 l2 = true -> compare_lists l2 l1 = true.
(* # *)
induction l1.
  destruct l2. 
  simpl; trivial.
  simpl; intro H; inversion H.

  destruct l2.
  simpl; intro H; inversion H.
  simpl.
  destruct (Nat.eq_dec a n).
  intro; rewrite e.
  destruct (Nat.eq_dec n n); simpl.
  apply IHl1; trivial.

  destruct n0; trivial.
  intro H; inversion H.
(* # *)
Qed.

(* reflexivité *)
Lemma compare_lists_r : forall l, compare_lists l l = true.
(* # *)
induction l;simpl;trivial.
destruct (Nat.eq_dec a a);trivial.
elim n;trivial.
Qed.
(* # *)



Definition compare_dl : dl -> dl -> bool :=
  fun d1 d2 => compare_lists (two2one d1) (two2one d2).


(* Sans surprise, le calcul qui est effectué par compare_lists en renvoyant
   un bool correspond à l'égalité sur les list nat : c'est prouvé par les deux
   lemmes suivants. *)
Lemma compare_bool_Prop : forall l1 l2, compare_lists l1 l2 = true -> l1=l2.
(* # *)
induction l1.
destruct l2. trivial.
simpl. intro H; inversion H.
destruct l2. simpl. intro H; inversion H.
simpl. destruct (Nat.eq_dec a n); simpl.
rewrite e. intro. replace l2 with l1; trivial.
apply IHl1; trivial.
intro H; inversion H.
(* # *)
Qed.


Lemma compare_Prop_bool : forall l1 l2, l1=l2 -> compare_lists l1 l2 = true.
(* # *) intros. rewrite H; apply compare_lists_r. (* # *)
Qed.




(* Définissez une fonction qui calcule le retournement d'une liste. *)
Definition rev2 : dl -> dl := (* # *) fun d =>
  match d with (l1,l2) => (l2,l1) end. (* # *)

(* # *) (* Lemmes à deviner (ou à incorporer dans la preuve) *)
Lemma t2o_app : forall l1 l2, t2o l1 l2 = app (rev l1) l2.

induction l1;simpl;trivial.
intro;rewrite IHl1.
rewrite app_ass; simpl; trivial.
Qed.
(* # *)


(* # *) 
Lemma rev_t2o_aux : forall l1 l2,
   compare_lists (List.rev (t2o l1 l2)) (t2o l2 l1) = true.

intros;rewrite t2o_app; rewrite t2o_app.
rewrite distr_rev. rewrite rev_involutive.
apply compare_lists_r.
Qed.
(* # *)

(* Pour prouver le lemme suivant, vous pouvez introduire des lemmes
   intermédiaires, et aussi vous appuyer sur la librairie List de Coq. *)
Lemma rev2_two2one : forall d, 
   compare_lists (List.rev (two2one d)) (two2one (rev2 d))=true.
(* # *)
destruct d.
apply rev_t2o_aux. (* # *)
Qed.



(* Retirer un élément d'une liste *)

(*
Définir une fonction rid : dl -> nat -> dl, qui retire un élément d'une liste 
s'il le trouve, et renvoie la liste inchangée sinon.
Si l'élément apparaît plusieurs fois dans la liste, on n'en retire qu'une seule copie.

Veillez à prendre en compte les recommandations ci-dessus à propos du fait que l'on 
évite de parcourir inutilement des bouts de liste, et que l'on préfère que 
"ça penche à droite" plutôt qu'à gauche.
*)

(* # *)
Inductive found_notfound : Set :=
| found : dl -> found_notfound
| not_found : list nat -> found_notfound.  (* DH : l'argument est sans doute inutile *)

Fixpoint rid1 l k acc := match l with
| nil => not_found acc
| x::xs => if (Nat.eq_dec x k) then found (xs,acc) else rid1 xs k (x::acc) end.

Definition rid_aux d := fun k => match d with
| ([],l2) => rid1 l2 k []
| (l1,l2) => 
     let findl1 := rid1 l1 k l2 in match findl1 with
     | found _ => findl1
     | not_found _ => rid1 l2 k l1
end end.

Definition rid d k := match rid_aux d k with
| found d' => d'
| not_found _ => d end.
(* # *)


(*
Définir une fonction rid_sorted : dl -> nat -> dl, qui fait comme rid2, à ceci près 
que l'on suppose que la liste passée en argument (et représentée avec un dl) est triée.*)

(* # *)
Fixpoint rid_sorted1 l := fun k acc (way:bool) => match l with
| [] => not_found []
| x::xs => let f := found (xs,acc) in
          if way then if le_lt_dec x k then f else rid_sorted1 xs k (x::acc) way
          else if le_lt_dec k x then f else rid_sorted1 xs k (x::acc) way
    end.

Definition rid_sorted_aux d k := match d with
| ([],l2) => rid_sorted1 l2 k [] true
| (l1,l2) => 
    let ridl1 := rid_sorted1 l1 k l2 false in
    match ridl1 with
    | found _ => ridl1
    | not_found _ => rid_sorted1 l2 k l1 true 
end end.

Definition rid_sorted d k := match rid_sorted_aux d k with
| found d' => d'
| not_found _ => d end.
(* # *)



(* Relation inductive entre listes. *)

(* Définir une relation inductive dont l'intention est qu'elle coïncide avec compare_dl.
   Il n'est pas autorisé de faire appel à des fonctions définies ci-dessus dans la 
   définition de same. *)
Inductive same : dl -> dl -> Prop := (* # *)
  same_refl : forall d, same d d
| same_right : forall l1 l2 x, same (l1, x::l2) (x::l1, l2)
| same_trans : forall d1 d2 d3, same d1 d2 -> same d2 d3 -> same d1 d3 
| same_sym : forall d1 d2, same d1 d2 -> same d2 d1(* # *).
(* # *)
(* on peut peut-être incorporer right dans trans, pour faire des preuves plus "efficaces" *)
(* # *)



(* On prouve ensuite que compare_dl et same coïncident. *)


Theorem same_compare :
  forall d1 d2, same d1 d2 -> compare_dl d1 d2 = true.
(* # *)
intros d1 d2 H;unfold compare_dl;induction H.

apply compare_lists_r.
unfold t2o; simpl. apply compare_lists_r.
apply compare_lists_t with (two2one d2); trivial.
apply compare_lists_s; trivial.
(* # *)
Qed.
 


(*# *)
Lemma same_nil_rev : forall l1 l2, same (l1,l2) ([], rev l1 ++ l2).
induction l1.
  simpl. intro; apply same_refl.
  intro l2; apply same_trans with (l1,a::l2).
  apply same_sym; apply same_right.
  
  apply same_trans with ([], rev l1 ++ (a::l2)).
  apply IHl1.
  simpl. rewrite app_ass. simpl.
  apply same_refl.
Qed.


Lemma compare_lists_n : forall l1 l l2, 
   compare_lists l (rev l1 ++ l2) = true -> same ([],l) (l1,l2).

intros.
replace l with (rev l1 ++ l2).
apply same_sym; apply same_nil_rev.
apply compare_bool_Prop. apply compare_lists_s. trivial.
Qed.
(* # *)



Theorem compare_same :
  forall d1 d2, compare_dl d1 d2 = true -> same d1 d2.
(* # *)
unfold compare_dl. unfold two2one. destruct d1; destruct d2. 
rewrite t2o_app. rewrite t2o_app.
revert l2 l1 l0.
induction l. 
  simpl; intros.
  apply compare_lists_n; trivial.

  intros l2 l1 l0. simpl. rewrite app_ass. simpl. intro.
  apply same_trans with (l,a::l0). apply same_sym; apply same_right.
  apply IHl. trivial.
(* # *)
Qed.


(* fin du DM ***************************************************************)

(* below : rubbish, pas besoin de lire ****************************************************)




(* Décommentez la définition suivante, et voyez ce qui se passe.
Fixpoint two2one' (d:dl) := match d with
  ([], l2) => l2
| (x::xs, l2) => x::(two2one' (xs,l2)) end.
*)
(* Cette définition est refusée car (xs,l2) n'est pas un "sous-arbre" (on dit
   sous-terme) direct de d, et par conséquent Coq n'est pas capable de se
   convaincre que la fonction terminera toujours. 
   On procède par conséquent ainsi :*)

(* pour connaître de nombreux résultats utiles sur les entiers *)

(* pour définir des fonctions récursives de manière "avancée" (cf. ci-dessous) *)
Require Export Recdef.

Definition sizeleft := fun (d:dl) => 
  match d with (l1,l2) => length l1 end.

(* Function au lieu de Fixpoint, et "measure" suivi d'un nom
   de fonction et d'un nom d'argument *)
Function two2one' (d : dl)  {measure sizeleft d} :=
  match d with
| (nil,l2) => l2
| (x::xs,l2) => two2one' (xs,x::l2) end.
(* Pour accepter la définition, Coq nous demande de prouver un but (très simple). *)
intros. simpl. auto.
Qed.


Lemma two2one_curry : forall d, two2one d = two2one' d.

intro d.
 (* functional induction : do induction according to the recursive call structure *)
functional induction two2one' d. 
simpl; trivial.
rewrite <- IHl.
simpl; trivial.
Qed.


(* ici à titre instructif : à virer ? *)
Function merge d {measure sizetwo d} := 
  match d with
| (nil, l2) => l2
| (l1, nil) => l1
| (x::xs as l1, y::ys as l2) => 
       if le_lt_dec x y then x::(merge (xs,l2)) else y::(merge (l1,ys))
end.

intros. unfold sizetwo;simpl. auto with arith.
intros. unfold sizetwo;simpl. auto with arith.
Qed.






(* how useful is this lemma? *)
Lemma compare_lists_inv : forall x y xs ys, compare_lists (x::xs) (y::ys) = true
 -> x=y /\ compare_lists xs ys = true.
intros.
simpl in H.
destruct (Nat.eq_dec x y) in H.
auto.
inversion H.
Qed.



Require Export Lia.
Lemma same_length : forall d1 d2, same d1 d2 -> sizetwo d1 = sizetwo d2.

intros. induction H; trivial.
unfold sizetwo; simpl. lia.
rewrite IHsame1; rewrite <- IHsame2; trivial.
rewrite IHsame; trivial.
Qed.


Fixpoint insert_sorted_right (l:list nat) := fun k => 
  match l with
  | nil => k::nil
  | x::xs => if (lt_dec x k)
       then x::(insert_sorted_right xs k)
       else k::l end.

(* on pourrait faire une version generique de insert_sorted pour une liste, 
a laquelle on passe en argument la fonction de test *)
Fixpoint insert_sorted_left (l:list nat) := fun k => 
  match l with
  | nil => k::nil
  | x::xs => if lt_dec k x 
        then x::(insert_sorted_left xs k)
        else k::l end.


Inductive csame_r : list nat -> dl -> Prop :=
| csra : forall l, csame_r l ([],l)
| csrr : forall l0 l x xs, csame_r l0 (l,x::xs) -> csame_r l0 (x::l,xs)
.

Inductive csame : dl -> dl -> Prop :=
| csl : forall l l1 l2 l3 l4, csame_r l (l1,l2) -> csame_r l (l3,l4) -> 
          csame (l1,l2) (l3,l4).

(*
Lemma same_c : forall l1 l2 l0, same (l0,l1) (l0,l2) 
     -> forall k, same (l0,k::l1) (l0,k::l2).

induction l1.
destruct l2. intros; apply same_refl.
simpl; intros l0 H. 
absurd (sizetwo (l0,[]) = sizetwo (l0,n::l2)).
unfold sizetwo;simpl. lia. 
apply same_length; trivial.

destruct l2.
intros l0 H. absurd (sizetwo (l0,a::l1) = sizetwo (l0,[])).
unfold sizetwo;simpl;lia.
apply same_length; trivial.



intros.
cut (a=n).
intro Han; rewrite Han; rewrite Han in H.
apply same_trans with (k::l0, n::l1).
apply same_right.
apply same_trans with (k::l0,n::l2).
2:apply same_sym; apply same_right.
apply IHl1. 
Admitted.
*)


(*
Lemma same_l : forall d, same d ([],two2one d).

destruct d; simpl; rewrite t2o_app.
revert l0.
induction l; intro.

  simpl. apply same_refl.

  simpl. rewrite app_ass. simpl.
  apply same_trans with (l,a::l0).
  apply same_sym; apply same_right.
  apply IHl.
Qed.
*)



